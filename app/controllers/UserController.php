<?php

use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use JwtDecodeToken as decodeAuth;

class UserController extends ControllerBase {

    protected $infologger;
    protected $errorlogger;
    protected $payload;
    protected $moduleName;

    /**
     * createUsers
     * @return type
     */
    public function createUsers() {
        $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $string = (preg_replace('/"national_id":"[^"]*?"/', '"national_id":****ID****', $string) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | User Create "
                . "Request:" . ($string));

        $username = isset($data->username) ? $data->username : null;
        $phone = isset($data->phone) ? $data->phone : null;
        $password = isset($data->password) ? $data->password : null;

        $secretKey = "55abe029fdebae5e1d417e2ffb2a003klkhka0cd8b54763051cef08bc55abe029";

        $len = rand(1000, 999999);
        //$dataInfo = $this->randStrGen($len);

        $payloadToken = [ 'data' => $len . "" . $this->now()];

        $token = md5($this->createNewAuthToken($payloadToken, $secretKey));

        if (!$token || !$password || !$username) {
            return $this->unProcessable(__FUNCTION__ . ":" . __CLASS__);
        }
        if (!$phone) {
            return $this->dataError(__LINE__ . ":" . __CLASS__
                            , "Validation Error."
                            , ['code' => 422
                        , 'message' => 'Provide phone number.']);
        }

        $phone = $this->validateMobile($phone);
        if (!$phone) {
            return $this->dataError(__LINE__ . ":" . __CLASS__
                            , "Validation Error."
                            , ['code' => 422
                        , 'message' => "Invalid Phone Number Format"
                        . ". Kindly enter valid phone number"]);
        }
        $transactionManager = new TransactionManager();
        $dbTransaction = $transactionManager->get();
        try {
            $checkProfile = Profile::findFirst([
                        "phone =:phone:",
                        "bind" => [ "phone" => $phone],]);

            $profileId = isset($checkProfile->profile_id) ?
                    $checkProfile->profile_id : false;
            if (!$checkProfile) {
                $profile = new Profile();
                $profile->setTransaction($dbTransaction);
                $profile->created_at = $this->now();
                $profile->phone = $phone;
                $profile->network = $this->getMobileNetwork($phone);
                if ($profile->save() === false) {
                    $errors = [];
                    $messages = $profile->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTransaction->rollback("Create profile failed. Reason" . json_encode($errors));
                }

                $profileId = $profile->profile_id;
            }

            $checkUser = Users::findFirst([
                        "profile_id =:profile_id:",
                        "bind" => [ "profile_id" => $profileId],]);

            $user_id = isset($checkUser->user_id) ? $checkUser->user_id : false;
            /**
             * Create user
             */
            if (!$checkUser) {
                $user = new Users();
                $user->setTransaction($dbTransaction);
                $user->profile_id = $profileId;
                $user->username = $username;
                $user->token = $token;
                $user->role_id = 1;
                $user->password = $password;
                $user->created_at = $this->now();
                if ($user->save() === false) {
                    $errors = [];
                    $messages = $user->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTransaction->rollback("Create User failed. Reason" . json_encode($errors));
                }

                $userId = $user->user_id;
            }
           
            $dbTransaction->commit();

            $data_array['message'] = 'Account has been created succeesful';
            return $this->success(__LINE__ . ":" . __CLASS__
            , 'Account Created Successful'
            , $data_array);
        } catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.". $ex->getMessage());
        }
    }
    /**
     * createUsers
     * @return type
     */

    public function loginAction() {
        $request = new Request();
        $data = $request->getJsonRawBody();

        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"password":"[^"]*?"/';
        $string = (preg_replace($regex, '"password":*'
                        , json_encode($request->getJsonRawBody())) . PHP_EOL);
        $this->infologger->addInfo(_LINE_ . ":" . _CLASS_ . " | Login"
                . "Request::$string");

        $this->payload['salt'] = isset($data->salt_token) ? $data->salt_token : NULL;
        $this->payload['user_name'] = isset($data->user_name) ? $data->user_name : NULL;
        $this->payload['password'] = isset($data->password) ? $data->password : NULL;

        if (!$this->payload['password'] || !$this->payload['user_name'] || !$this->payload['salt']) {
            return $this->unProcessable(_LINE_ . ":" . _CLASS_);
        }

        /**
         * validate salt here
         */
        try {
            $result = $this->QueryUserUsingUsername($this->payload['user_name']);
            if (!$result) {
                return $this->unAuthorised(_LINE_ . ":" . _CLASS_, 'User DOES NOT Exist!');
            }
            $password = $this->payload['password'];
            if (md5($password) != md5($result["password"])) {

                return $this->unAuthorised(_LINE_ . ":" . _CLASS_
                                , 'An Error Occured, Invalid Username or Password!'.md5($password).' db '.md5($result["password"]));
            }

            return $this->success(_LINE_ . ":" . _CLASS_
                            , 'Request Successful'
                            , ['code' => 200
                        , 'message' => 'Access has been granted to your account'
                        , 'data' => ['key' => $result['token']]]);
        } catch (Exception $ex) {
            $this->errorlogger->emergency(_LINE_ . ":" . _CLASS_ . " | "
                    . "Exception::" . $ex->getMessage());
            return $this->serverError(_LINE_ . ":" . _CLASS_
                            , 'Internal Server Error!');
        }
    } 

    

}
