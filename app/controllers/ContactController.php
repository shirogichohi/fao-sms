<?php

use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use JwtDecodeToken as decodeAuth;

class ContactController extends ControllerBase {
    protected $infologger;
    protected $errorlogger;
    protected $payload;
    protected $moduleName;

    /**
     * createUsers
     * @return type
     */
    public function createCampaign() {
        $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $string = (preg_replace('/"national_id":"[^"]*?"/', '"national_id":****ID****', $string) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | Campaign Create "
                . "Request:" . ($string));
        
        $token = isset($data->apiKey) ? $data->apiKey : null;
        $campaign_description = isset($data->campaign_description) ? $data->campaign_description : null;

        if (!$token) {
            return $this->unProcessable(__FUNCTION__ . ":" . __CLASS__);
        }
        try{
            $authResponse = Authenticate
                    ::QuickTokenAuthenticate($token);
            if (!$authResponse) {
                return $this->unAuthorised(__LINE__ . ":" . __CLASS__
                                , 'Authentication Failure.');
            }

            $checkCampaign = Campaign::findFirst([
                        "campaign_description =:campaign_description:",
                        "bind" => [ "campaign_description" => $campaign_description],]);
            if($checkCampaign){
                return $this->dataError(__LINE__ . ":" . __CLASS__
                                , "Campaign Exist", []);
            }
            $transactionManager = new TransactionManager();
            $dbTransaction = $transactionManager->get();
            try{
                $newCampaign = new Campaign();
                $newCampaign->setTransaction($dbTransaction);
                $newCampaign->campaign_description = $campaign_description;
                $newCampaign->created_at = $this->now();
                if ($newCampaign->save() === false) {
                    $errors = [];
                    $messages = $newCampaign->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTransaction->rollback("Create Campaign failed. Reason" . json_encode($errors));
                }

                $campaign_id = $newCampaign->campaign_id;
                $dbTransaction->commit();
                $data_array['message'] = 'Campaign has been created succeesful';
                $data_array['id'] = $campaign_id;
                return $this->success(__LINE__ . ":" . __CLASS__
                , 'Campaign Created Successful'
                , $data_array);
            }
            catch (Exception $ex) {
                throw $ex;
            }
        }
        catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.". $ex->getMessage());
        }
    }
    /**
     * createContacts
     * @return type
     */
    public function createContact(){
        $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $string = (preg_replace('/"national_id":"[^"]*?"/', '"national_id":****ID****', $string) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | Contact Create "
                . "Request:" . ($string));
        
        $token = isset($data->apiKey) ? $data->apiKey : null;
        $fullname = isset($data->fullname) ? $data->fullname : null;
        $campaign_id = isset($data->campaign_id) ? $data->campaign_id : null;
        $phone = isset($data->phone) ? $data->phone : null;

        if (!$token || !$campaign_id || !$phone || !$fullname) {
            return $this->unProcessable(__FUNCTION__ . ":" . __CLASS__);
        }
        $transactionManager = new TransactionManager();
        $dbTransaction = $transactionManager->get();
        try{
            $authResponse = Authenticate
                    ::QuickTokenAuthenticate($token);
            if (!$authResponse) {
                return $this->unAuthorised(__LINE__ . ":" . __CLASS__
                                , 'Authentication Failure.');
            }

            $checkProfile = Profile::findFirst([
                        "phone =:phone:",
                        "bind" => [ "phone" => $phone],]);
            if($checkProfile){
                
                $profileId = $checkProfile->profile_id;
            }
            
            else{
                $profile = new Profile();
                $profile->setTransaction($dbTransaction);
                $profile->created_at = $this->now();
                $profile->phone = $phone;
                $profile->network = $this->getMobileNetwork($phone);
                if ($profile->save() === false) {
                    $errors = [];
                    $messages = $profile->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTransaction->rollback("Create profile failed. Reason" . json_encode($errors));
                }

                $profileId = $profile->profile_id;
            }
             $checkContact = Contact::findFirst([
                        "campaign_id =:campaign_id: AND profile_id= :profile_id:",
                        "bind" => [ "campaign_id" => $campaign_id, "profile_id"=>$profileId]]);
            if($checkContact){
                return $this->dataError(__LINE__ . ":" . __CLASS__
                                , "Contact Exist", []);
            }
            $contact = new Contact();
            $contact->setTransaction($dbTransaction);
            $contact->created_at = $this->now();
            $contact->phone = $phone;
            $contact->fullname = $fullname;
            $contact->campaign_id = $campaign_id;
            $contact->profile_id = $profileId;
            if ($contact->save() === false) {
                $errors = [];
                $messages = $contact->getMessages();
                foreach ($messages as $message) {
                    $e["statusDescription"] = $message->getMessage();
                    $e["field"] = $message->getField();
                    array_push($errors, $e);
                }

                $dbTransaction->rollback("Create contact failed. Reason" . json_encode($errors));
            }
            $dbTransaction->commit();
            $data_array['message'] = 'contact has been created succeesful';
            $data_array['id'] = $contact->contact_id;
            return $this->success(__LINE__ . ":" . __CLASS__
            , 'Contact Created Successful'
            , $data_array);
           
        }
        catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.". $ex->getMessage());
        }
         
    }
    /**
     * viewCampaign
     */

    public function viewContact(){
        $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | Contact Create "
                . "Request:" . ($string));
        
        $token = isset($data->apiKey) ? $data->apiKey : null;
        if (!$token ) {
            return $this->unProcessable(__LINE__ . ":" . __CLASS__);
        }

        try {
            $authResponse = Authenticate
                    ::QuickTokenAuthenticate($token);
            if (!$authResponse) {
                return $this->unAuthorised(__LINE__ . ":" . __CLASS__
                                , 'Authentication Failure.');
            }

            $sql = "SELECT * FROM contact c join profile  p on c.profile_id = p.profile_id";
            $result = $this->rawSelect($sql);
            if (empty($result)) {

                $stop = $this->getMicrotime() - $start_time;
                return $this->success(__LINE__ . ":" . __CLASS__, 'No Record Found', [
                            'code' => 404,
                            'sql' => $sql,
                            'message' => "Query returned no results ( $stop Seconds)",
                            'data' => [],
                            'record_count' => 0], true);
            }
            $stop = $this->getMicrotime() - $start_time;
            return $this->successLarge(__LINE__ . ":" . __CLASS__, 'Ok', [
                        'code' => 200,
                        'record_count' => count($result),
                        'message' => "Query returned results ( $stop Seconds)",
                        'data' => $result,]);
            }
         catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.". $ex->getMessage());
        }

    }


      public function viewCampaign(){
        $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | Contact Create "
                . "Request:" . ($string));
        
        $token = isset($data->apiKey) ? $data->apiKey : null;
        if (!$token ) {
            return $this->unProcessable(__LINE__ . ":" . __CLASS__);
        }

        try {
            $authResponse = Authenticate
                    ::QuickTokenAuthenticate($token);
            if (!$authResponse) {
                return $this->unAuthorised(__LINE__ . ":" . __CLASS__
                                , 'Authentication Failure.');
            }

            $sql = "SELECT * FROM campaign";
            $result = $this->rawSelect($sql);
            if (empty($result)) {

                $stop = $this->getMicrotime() - $start_time;
                return $this->success(__LINE__ . ":" . __CLASS__, 'No Record Found', [
                            'code' => 404,
                            'sql' => $sql,
                            'message' => "Query returned no results ( $stop Seconds)",
                            'data' => [],
                            'record_count' => 0], true);
            }
            $stop = $this->getMicrotime() - $start_time;
            return $this->successLarge(__LINE__ . ":" . __CLASS__, 'Ok', [
                        'code' => 200,
                        'record_count' => count($result),
                        'message' => "Query returned results ( $stop Seconds)",
                        'data' => $result,]);
            }
             catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.". $ex->getMessage());
        }

    }

}