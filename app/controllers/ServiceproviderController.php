<?php

use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use JwtDecodeToken as decodeAuth;

class ServiceproviderController extends ControllerBase {
    protected $infologger;
    protected $errorlogger;
    protected $payload;
    protected $moduleName;

      /**
     * createServiceprovider
     * @return type
     */
    public function createServiceprovider() {
        $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | Campaign Create "
                . "Request:" . ($string));
        
        $token = isset($data->apiKey) ? $data->apiKey : null;
        $sp_name = isset($data->sp_name) ? $data->sp_name : null;
        $username = isset($data->username) ? $data->username : null;
        $SPAtoken = isset($data->SPAtoken) ? $data->SPAtoken : null;


        if (!$token || !$sp_name || !$SPAtoken) {
            return $this->unProcessable(__FUNCTION__ . ":" . __CLASS__);
        }
        try{
            $authResponse = Authenticate
                    ::QuickTokenAuthenticate($token);
            if (!$authResponse) {
                return $this->unAuthorised(__LINE__ . ":" . __CLASS__
                                , 'Authentication Failure.');
            }

            
            $transactionManager = new TransactionManager();
            $dbTransaction = $transactionManager->get();
            try{
                $checkServiceprovider = Serviceprovider::findFirst([
                        "sp_name =:sp_name:",
                        "bind" => [ "sp_name" => $sp_name],]);
            if($checkServiceprovider){
                $sp_id = $checkServiceprovider->sp_id;
            }
            else{
                 $newServiceprovider = new Serviceprovider();
                $newServiceprovider->setTransaction($dbTransaction);
                $newServiceprovider->sp_name = $sp_name;
                $newServiceprovider->created_at = $this->now();
                if ($newServiceprovider->save() === false) {
                    $errors = [];
                    $messages = $newServiceprovider->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTransaction->rollback("Create Service Provider failed. Reason" . json_encode($errors));
                }

                $sp_id = $newServiceprovider->sp_id;
            }
               
                $checkSP = SpAuth::findFirst([
                        "sp_id =:sp_id:",
                        "bind" => [ "sp_id" => $sp_id],]);
                if($checkSP){
                     return $this->dataError(__LINE__ . ":" . __CLASS__
                                , "Service Provider Exist", []);
                }
                if (!$checkSP) {
                $spAuth = new SpAuth();
                $spAuth->setTransaction($dbTransaction);
                $spAuth->api_key = $SPAtoken;
                $spAuth->auth = $username;
                $spAuth->sp_id = $sp_id;
                $spAuth->created_at = $this->now();

                if ($spAuth->save() === false) {
                    $errors = [];
                    $messages = $spAuth->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTransaction->rollback("Create Service Provider Auth  failed. Reason" . json_encode($errors));
                }

                $dbTransaction->commit();
                $data_array['message'] = 'Service Provider has been created succeesful';
                $data_array['id'] = $spAuth->sp_autn_id;
                return $this->success(__LINE__ . ":" . __CLASS__
                , 'Service Provider Created Successful'
                , $data_array);
            }
        }
            catch (Exception $ex) {
                throw $ex;
            }
        }
        catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.". $ex->getMessage());
        }
    }
    /**
     * viewServicesProvider
     */
    public function viewServicesProvider(){
         $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | View Providers "
                . "Request:" . ($string));
        
        $token = isset($data->apiKey) ? $data->apiKey : null;
        if (!$token ) {
            return $this->unProcessable(__LINE__ . ":" . __CLASS__);
        }

        try {
            $authResponse = Authenticate
                    ::QuickTokenAuthenticate($token);
            if (!$authResponse) {
                return $this->unAuthorised(__LINE__ . ":" . __CLASS__
                                , 'Authentication Failure.');
            }

            $sql = "SELECT * FROM Serviceprovider c join Sp_auth  s on c.sp_id = s.sp_id";
            $result = $this->rawSelect($sql);
            if (empty($result)) {

                $stop = $this->getMicrotime() - $start_time;
                return $this->success(__LINE__ . ":" . __CLASS__, 'No Record Found', [
                            'code' => 404,
                            'sql' => $sql,
                            'message' => "Query returned no results ( $stop Seconds)",
                            'data' => [],
                            'record_count' => 0], true);
            }
            $stop = $this->getMicrotime() - $start_time;
            return $this->successLarge(__LINE__ . ":" . __CLASS__, 'Ok', [
                        'code' => 200,
                        'record_count' => count($result),
                        'message' => "Query returned results ( $stop Seconds)",
                        'data' => $result,]);
            }
             catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.". $ex->getMessage());
        }
    }
}