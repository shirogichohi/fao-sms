<?php

use AfricasTalking\SDK\AfricasTalking;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use JwtDecodeToken as decodeAuth;

class sendSMSController extends ControllerBase {
    protected $infologger;
    protected $errorlogger;
    protected $payload;
    protected $moduleName;
    /**
     * sendSMSSingle
     * @return type
     */
    public function sendSMSSingle(){
        $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | Single SMS "
                . "Request:" . ($string));
        
        $token = isset($data->apiKey) ? $data->apiKey : null;
        $phone = isset($data->phone) ? $data->phone : null;
        $message = isset($data->message) ? $data->message : null;
        $sp_id = isset($data->sp_id) ? $data->sp_id : null;
        $sp_name = isset($data->sp_name) ? $data->sp_name : null;
        $save = isset($data->save) ? $data->save : 0;

        if (!$token || !$phone || !$sp_id || !$message) {
                return $this->unProcessable(__FUNCTION__ . ":" . __CLASS__);
        }
        $phone = $this->validateMobile($phone);
        if (!$phone) {
            return $this->dataError(__LINE__ . ":" . __CLASS__
                            , "Validation Error."
                            , ['code' => 422
                        , 'message' => "Invalid Phone Number Format"
                        . ". Kindly enter valid phone number"]);
        }
        try{
                $authResponse = Authenticate
                        ::QuickTokenAuthenticate($token);
                if (!$authResponse) {
                    return $this->unAuthorised(__LINE__ . ":" . __CLASS__
                                    , 'Authentication Failure.');
                }
                $checkSP = SpAuth::findFirst([
                            "sp_id =:sp_id:",
                            "bind" => [ "sp_id" => $sp_id],]);
                if(!$checkSP){
                    return $this->dataError(__LINE__ . ":" . __CLASS__
                                    , "Service Provider Does Not Exist", []);
                }
                $transactionManager = new TransactionManager();
                $dbTransaction = $transactionManager->get();
                try{
                    $messageNew = new Message();
                    $messageNew->setTransaction($dbTransaction);
                    $messageNew->description = $message;
                    $messageNew->save_as_template = $save;
                    $messageNew->created_at = $this->now();
                    if ($messageNew->save() === false) {
                        $errors = [];
                        $messages = $messageNew->getMessages();
                        foreach ($messages as $message) {
                            $e["statusDescription"] = $message->getMessage();
                            $e["field"] = $message->getField();
                            array_push($errors, $e);
                        }

                        $dbTransaction->rollback("Create Message failed. Reason" . json_encode($errors));
                    }

                    $messageID = $messageNew->message_id;
                    $checkProfile = Profile::findFirst([
                        "phone =:phone:",
                        "bind" => [ "phone" => $phone],]);
                    if($checkProfile){
                        
                        $profileId = $checkProfile->profile_id;
                    }
                    
                    else{
                        $profile = new Profile();
                        $profile->setTransaction($dbTransaction);
                        $profile->created_at = $this->now();
                        $profile->phone = $phone;
                        $profile->network = $this->getMobileNetwork($phone);
                        if ($profile->save() === false) {
                            $errors = [];
                            $messages = $profile->getMessages();
                            foreach ($messages as $message) {
                                $e["statusDescription"] = $message->getMessage();
                                $e["field"] = $message->getField();
                                array_push($errors, $e);
                            }

                            $dbTransaction->rollback("Create profile failed. Reason" . json_encode($errors));
                        }

                        $profileId = $profile->profile_id;
                    }
                    $outbox = new Outbox();
                    $outbox->setTransaction($dbTransaction);
                    $outbox->status = "SENT";
                    $outbox->message_id = $messageID;
                    $outbox->sp_id = $sp_id;
                    $outbox->profile_id = $profileId;
                    $outbox->created_at = $this->now();
                    if ($outbox->save() === false) {
                        $errors = [];
                        $messages = $outbox->getMessages();
                        foreach ($messages as $message) {
                            $e["statusDescription"] = $message->getMessage();
                            $e["field"] = $message->getField();
                            array_push($errors, $e);
                        }

                        $dbTransaction->rollback("Create Outbox failed. Reason" . json_encode($errors));
                    }

                    $outboxId = $outbox->id;

                    if($sp_name == 'Africastalking'){
                        $username = $checkSP->auth; // use 'sandbox' for development in the test environment
                        $apiKey   = $checkSP->api_key; // use your sandbox app API key for development in the test environment
                        $AT       = new AfricasTalking($username, $apiKey);

                        // Get one of the services
                        $sms      = $AT->sms();

                        // Use the service
                        $result   = $sms->send([
                            'to'      => $phone,
                            'message' => $message
                        ]);
                       
                    }
                    /**
                     * add other services providers 
                     * todo
                     */
                    $data = json_decode($result);
                    $outboxDLR = new OutboxDlr();
                    $outboxDLR->setTransaction($dbTransaction);
                    $outboxDLR->status = "Success";
                    $outboxDLR->outbox_id = $outboxId;
                    $outboxDLR->meta_data = $data;
                    $outboxDLR->delivered_at = $this->now();
                    if ($outboxDLR->save() === false) {
                        $errors = [];
                        $messages = $outboxDLR->getMessages();
                        foreach ($messages as $message) {
                            $e["statusDescription"] = $message->getMessage();
                            $e["field"] = $message->getField();
                            array_push($errors, $e);
                        }

                        $dbTransaction->rollback("Create Outbox failed. Reason" . json_encode($errors)."status".$result->SMSMessageData->Recipients[0]['status']);
                    }
                     $dbTransaction->commit();
                     return $this->success(__LINE__ . ":" . __CLASS__
                        , 'Campaign Created Successful'
                        , ["data"=>$result]);
                 }
                 catch (Exception $ex) {
                     throw $ex;
                 }
                

        }
        catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.". $ex->getMessage());
        }
    }

    /**
     * viewSMS
     */
    public function viewSMS(){
        $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | Single SMS "
                . "Request:" . ($string));
        
        $token = isset($data->apiKey) ? $data->apiKey : null;
        if (!$token ) {
            return $this->unProcessable(__FUNCTION__ . ":" . __CLASS__);
        }
         try {
            $authResponse = Authenticate
                    ::QuickTokenAuthenticate($token);
            if (!$authResponse) {
                return $this->unAuthorised(__LINE__ . ":" . __CLASS__
                                , 'Authentication Failure.');
            }

            $sql = "SELECT * FROM message c join outbox  p on c.message_id = p.message_id"
                    ." join outbox_dlr d on p.id = d.outbox_id";
            $result = $this->rawSelect($sql);
            if (empty($result)) {

                $stop = $this->getMicrotime() - $start_time;
                return $this->success(__LINE__ . ":" . __CLASS__, 'No Record Found', [
                            'code' => 404,
                            'sql' => $sql,
                            'message' => "Query returned no results ( $stop Seconds)",
                            'data' => [],
                            'record_count' => 0], true);
            }
            $stop = $this->getMicrotime() - $start_time;
            return $this->successLarge(__LINE__ . ":" . __CLASS__, 'Ok', [
                        'code' => 200,
                        'record_count' => count($result),
                        'message' => "Query returned results ( $stop Seconds)",
                        'data' => $result,]);
            }
         catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.". $ex->getMessage());
        }

    }
    
}