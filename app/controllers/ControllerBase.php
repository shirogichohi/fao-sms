<?php

ini_set("date.timezone", "Africa/Nairobi");

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\LineFormatter;
use \Firebase\JWT\JWT as jwt;
use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Phalcon\Mvc\Dispatcher as MvcDispatcher;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class ControllerBase extends Controller {

    /**
     * cmp
     * @param type $a
     * @param type $b
     * @return type
     */
    public function cmp($a, $b) {
        return strcmp($b['favourite'], $a['favourite']);
    }

    public function QueryUserUsingUsername($username) {
        
        try {
            $selectSql = "SELECT users.`user_id`,profile.phone,profile.network,users.`password`"
                    . ",users.`created_at`, users.token FROM `users` "
                    . "JOIN profile ON users.profile_id=profile.profile_id "
                    . "WHERE users.username=:username ";
            $result = $this->rawSelect($selectSql, [':username' => $username]);

            return isset($result[0]) ? $result[0] : false;
        } catch (Exception $ex) {
            $this->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | QueryUserUsingUsername Exception::" . $ex->getMessage());
        }

        return false;
    }

    /**
     * Profile
     * @param type $mobile
     * @return boolean
     * @throws Exception
     */
    public function Profile($mobile, $origin) {
        $result = false;

        try {
            $p = Profile::findFirst([ "msisdn=:msisdn:",
                        "bind" => [ "msisdn" => $mobile],]);

            $profile_id = isset($p->profile_id) ? $p->profile_id : 0;
            if ($profile_id > 0) {
                $pxs = ProfileAttribution::findFirst([ "profile_id=:profile_id:",
                            "bind" => [ "profile_id" => $profile_id],]);

                $transactionManager = new TransactionManager();
                $dbTrxn = $transactionManager->get();
                try {
                    if (!isset($pxs->id)) {
                        $prx = new ProfileAttribution();
                        $prx->setTransaction($dbTrxn);
                        $prx->profile_id = $profile_id;
                        $prx->last_use_date = $this->now();
                        $prx->created_at = $this->now();
                        $prx->frequency_of_use = 1;
                        $prx->origin = $origin;
                        if ($prx->save() === false) {
                            $errors = [];
                            $messages = $prx->getMessages();
                            foreach ($messages as $message) {
                                $e["statusDescription"] = $message->getMessage();
                                $e["field"] = $message->getField();
                                array_push($errors, $e);
                            }

                            $dbTrxn->rollback("Create Profile Attribution failed "
                                    . json_encode($errors));
                        }

                        $dbTrxn->commit();

                        return $profile_id;
                    }

                    $pxs->setTransaction($dbTrxn);
                    $pxs->frequency_of_use = ($pxs->frequency_of_use + 1);
                    $pxs->last_use_date = $this->now();
                    if ($pxs->save() === false) {
                        $errors = [];
                        $messages = $pxs->getMessages();
                        foreach ($messages as $message) {
                            $e["statusDescription"] = $message->getMessage();
                            $e["field"] = $message->getField();
                            array_push($errors, $e);
                        }

                        $dbTrxn->rollback("Update Profile Attribution failed "
                                . json_encode($errors));
                    }

                    $dbTrxn->commit();

                    return $profile_id;
                } catch (Exception $ex) {
                    throw $ex;
                }
            }

            $transactionManager = new TransactionManager();
            $dbTrxn = $transactionManager->get();
            try {
                $px = new Profile();
                $px->setTransaction($dbTrxn);
                $px->created_at = $this->now();
                $px->msisdn = $mobile;
                $px->network = $this->getMobileNetwork($mobile);
                $px->status = 1;
                if ($px->save() === false) {
                    $errors = [];
                    $messages = $px->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTrxn->rollback("Create Profile failed " . json_encode($errors));
                }

                $profile_id = $px->profile_id;

                $pxs = ProfileAttribution::findFirst([ "profile_id=:profile_id:",
                            "bind" => [ "profile_id" => $profile_id],]);
                if (isset($pxs->id)) {
                    $pxs->setTransaction($dbTrxn);
                    $pxs->frequency_of_use = ($pxs->frequency_of_use + 1);
                    $pxs->last_use_date = $this->now();
                    if ($pxs->save() === false) {
                        $errors = [];
                        $messages = $pxs->getMessages();
                        foreach ($messages as $message) {
                            $e["statusDescription"] = $message->getMessage();
                            $e["field"] = $message->getField();
                            array_push($errors, $e);
                        }

                        $dbTrxn->rollback("Update Profile Attribution failed "
                                . json_encode($errors));
                    }

                    $dbTrxn->commit();

                    return $profile_id;
                }

                $prx = new ProfileAttribution();
                $prx->setTransaction($dbTrxn);
                $prx->profile_id = $profile_id;
                $prx->last_use_date = $this->now();
                $prx->created_at = $this->now();
                $prx->frequency_of_use = 1;
                $prx->origin = $origin;
                if ($prx->save() === false) {
                    $errors = [];
                    $messages = $prx->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTrxn->rollback("Create Profile Attribution failed " . json_encode($errors));
                }

                $dbTrxn->commit();

                return $profile_id;
            } catch (Exception $ex) {
                throw $ex;
            }
        } catch (Exception $ex) {
            throw $ex;
        }

        return $result;
    }

//    public $moduleName;
//
//    /**
//     * 
//     * @param Dispatcher $dispatcher
//     */
//    public function beforeExecuteRoute(Dispatcher $dispatcher) {
//        $this->moduleName = $dispatcher->getControllerClass();
//
//        $this->getLogFile("info")->info(__LINE__ . ":" . __CLASS__ . " | The controler" . json_encode($dispatcher));
//
//        return $dispatcher;
//    }

    /**
     * getClientIPServer
     * @return string
     */
    public function getClientIPServer() {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if ($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if ($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if ($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    /**
     * 
     * @param type $msisdn
     * @param type $message
     * @param type $senderId
     * @return int
     */
   

    /**
     * permissionVerfication
     * @param type $params
     * @return boolean
     */
    public function permissionVerfication($params) {
        $permissionType = $params['permissionType'];
        $stringQuery = "";
        if (isset($params['verficationCode'])) {
            $verficationCode = $params['verficationCode'];
            $stringQuery = "and verification_code='$verficationCode'";
        }

        $token_details = Authenticate::AuthenticateTokenKey($params['token']);
        $t_details = $token_details && count($token_details) != 0 ? $token_details[0] : null;
        if (empty($token_details)) {
            return false;
        }

        $user_mapId = $t_details['user_mapId'];
        $user_id = $t_details['user_id'];

        $selectSql = "SELECT user_login_id FROM user_login WHERE user_id='$user_id' $stringQuery";
        $resultSQL = $this->rawSelect($selectSql);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " - | SQL:$selectSql");
        if (empty($resultSQL)) {
            return false;
        }
        try {
            // check if the verification code matches

            $params = [
                'user_mapId' => $user_mapId,
                'permissionType' => $permissionType
            ];
            return $this->userPermission($params);
        } catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "| Exception:" . $ex->getMessage());
            return $response->serverError(__LINE__ . ":" . __CLASS__, "Exception condition occured" . $ex->getMessage());
        }
    }

    /**
     * userPermission
     * @param type $params
     * @return boolean
     */
    public function userPermission($params) {
        $user_mapId = $params['user_mapId'];
        $permissionType = $params['permissionType'];
        // get  permission id
        $sqlGetPerm = $this->rawSelect("SELECT permission_id from "
                . "user_permission where permission='$permissionType'");
        $permIDNew = $sqlGetPerm[0]['permission_id'];

        try {
            $sqlResult = $this->rawSelect("SELECT permission_acl FROM "
                    . "user_client_map WHERE user_mapId='$user_mapId'");
            $permissions = $sqlResult[0]['permission_acl'];
            $permIDs = explode(':', $permissions);
            $userRightModel = array();
            foreach ($permIDs as $permID) {
                // check the type if permission
                if ($permID == $permIDNew) {
                    return true;
                }
            }
            return false;
        } catch (Exception $ex) {
            $this->getLogFile('error')->addEmergency(__LINE__ . ":" . __CLASS__
                    . " | Exception:" . $ex->getMessage());
            return false;
        }
    }

    public function randStrGen($len) {
        $result = "";
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789$11";
        $charArray = str_split($chars);
        for ($i = 0; $i < $len; $i++) {
            $randItem = array_rand($charArray);
            $result .= "" . $charArray[$randItem];
        }
        return $result;
    }

    /**
     * QueueMessage
     * @param type $payload
     */
   
    /**
     * formatMobileNumber
     * @param type $mobile
     * @return type
     */
    public function formatMobileNumber($mobile) {
        $mobile = str_replace("+", "", $mobile);
        $mobile = preg_replace('/\s+/', '', $mobile);
        $input = substr($mobile, 0, -strlen($mobile) + 1);
        $number = '';
        if ($input == '0') {
            $number = substr_replace($mobile, '254', 0, 1);

            return $number;
        } elseif ($input == '+') {
            $number = substr_replace($mobile, '', 0, 1);
        } elseif ($input == '7') {
            $number = substr_replace($mobile, '2547', 0, 1);
        } else {
            $number = $mobile;
        }
        return $number;
    }

    /**
     * validateMobile
     * @param type $number
     * @return boolean
     */
    public function validateMobile($number) {
        $regex = '/^(?:\+?(?:[1-9]{3})|0)?7([0-9]{8})$/';
        if (preg_match_all($regex, $number, $capture)) {
            $msisdn = '2547' . $capture[1][0];
        } else {
            $msisdn = false;
        }
        return $msisdn;
    }

    /**
     * Mysql Function
     */
    /*
     * raw insert
     */

    public function rawInsert($phql, $params = null) {
        try {
            $this->db->execute($phql, $params);
            $last_insert_id = $this->db->lastInsertId();
            return $last_insert_id;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param type $statement
     * @return type $quer
     *  resultset     
     */
    public function rawUpdate($statement) {
        try {
            $connection = $this->di->getShared("db");
            $success = $connection->execute($statement);
            $rowCount = $connection->affectedRows();
            return $rowCount;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param type $statement 
     * @return type $quer resultset
     */
    public function rawSelect($statement, $params = null, $db = null) {
        try {
            if ($db == null) {
                $connection = $this->di->getShared("db");
            } else {
                $connection = $this->di->getShared("db2");
            }

            $success = $connection->query($statement, $params);
            $success->setFetchMode(Phalcon\Db::FETCH_ASSOC);
            $result = $success->fetchAll($success);

            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * selectQuery
     * @param type $sql
     * @return type
     */
    public function selectQuery($sql) {
        try {
            $connection = $this->di->getShared("db2");
            $success = $connection->query($sql);
            $success->setFetchMode(Phalcon\Db::FETCH_ASSOC);
            $result = $success->fetchAll($success);

            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * tableQueryBuilder
     * @param type $sort
     * @param type $order
     * @param int $page
     * @param int $limit
     * @param type $groupBy
     * @return type
     */
    public function tableQueryBuilder($sort = "", $order = "", $page = 0, $limit = 10, $groupBy = "") {

        $orderBy = $sort ? "ORDER BY $sort $order" : "";

        $sortClause = "$groupBy $orderBy";

        if (!$page || $page <= 0) {
            $page = 1;
        }
        if (!$limit) {
            $limit = 10;
        }

        $ofset = (int) ($page - 1) * $limit;
        $limitQuery = "LIMIT $ofset, $limit";

        return "$sortClause $limitQuery";
    }

    /**
     * whereQuery
     * @param type $whereArray
     * @param type $groupBy
     * @param type $searchColumns
     * @return type
     */
    public function whereQuery($whereArray, $groupBy, $searchColumns = []) {

        $whereQuery = "";
        $havingQuery = "";

        foreach ($whereArray as $key => $value) {

            if ($key == 'filter') {
                $valueString = "";
                foreach ($searchColumns as $searchColumn) {
                    $valueString .= $value ? "" . $searchColumn . " REGEXP '" . $value . "' ||" : "";
                }
                $valueString = chop($valueString, " ||");
                if ($valueString) {
                    $valueString = "(" . $valueString;
                    $valueString .= ") AND ";
                }
                $whereQuery .= $valueString;
            } else if ($key == 'having') {
                if (!empty($value[1]) && !empty($value[2])) {
                    $valueString = " $value[0] between $value[1] AND $value[2] AND ";
                    $havingQuery .= $valueString;
                }
            } else if (is_array($value)) {
                $type = isset($value[3]) ? $value[3] : 1;

                if (!empty($value[1]) && !empty($value[2])) {
                    $valueString = $type == 1 ? " $value[0] between '$value[1]' AND '$value[2]' AND " : " $value[0] between $value[1] AND $value[2] AND ";
                    $whereQuery .= $valueString;
                }
            } else {
                $valueString = $value ? "" . $key . "=" . $value . " AND " : "";
                $whereQuery .= $valueString;
            }
        }

        if ($whereQuery) {
            $whereQuery = chop($whereQuery, " AND ");
        }

        if ($havingQuery) {
            $havingQuery = chop($havingQuery, " AND ");
        }

        $whereQuery = $whereQuery ? "WHERE $whereQuery " : "";
        $havingQuery = $havingQuery ? " HAVING $havingQuery " : "";

        return $whereQuery . $groupBy . $havingQuery;
    }

  

    /**
     * Gets the log file to use
     * @param type $action
     * @return type
     */
    public function getLogFile($action = "") {
        $logger = '';
        /**
         * Read the configuration
         */
        $logPathLocation = $this->logPath['location'];
        $dateFormat = $this->logPath['dateFormat'];
        $output = $this->logPath['output'];
        $filename = $this->logPath['systemName'];

        switch ($action) {
            case 'info':
                $streamFile = $logPathLocation . "" . $filename . "Info.log";
                $stream = new StreamHandler($streamFile, Logger::INFO);
                $stream->setFormatter(new LineFormatter($output, $dateFormat));
                $logger = new Logger('INFO');
                $logger->pushHandler($stream);
                break;
            case 'error':
                $streamFile = $logPathLocation . "" . $filename . "Error.log";
                $stream = new StreamHandler($streamFile, Logger::ERROR);
                $stream->setFormatter(new LineFormatter($output, $dateFormat));
                $logger = new Logger('ERROR');
                $logger->pushHandler($stream);
                break;
            case 'fatal':
                $streamFile = $logPathLocation . "" . $filename . "Fatal.log";
                $stream = new StreamHandler($streamFile, Logger::EMERGENCY);
                $stream->setFormatter(new LineFormatter($output, $dateFormat));
                $logger = new Logger('FATAL');
                $logger->pushHandler($stream);
                break;
            case 'debug':
                $streamFile = $logPathLocation . "" . $filename . "Debug.log";
                $stream = new StreamHandler($streamFile, Logger::DEBUG);
                $stream->setFormatter(new LineFormatter($output, $dateFormat));
                $logger = new Logger('DEBUG');
                $logger->pushHandler($stream);
                break;
            default:
                $streamFile = $logPathLocation . "" . $filename . "Api.log";
                $stream = new StreamHandler($streamFile, Logger::INFO);
                $stream->setFormatter(new LineFormatter($output, $dateFormat));
                $logger = new Logger('INFO');
                $logger->pushHandler($stream);
                break;
        }

        return $logger;
    }

    /**
     * clean variable string
     * 
     * @param type $text
     * @return type
     */
    public function cleanMessageString($text) {
        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u' => 'A',
            '/[ÍÌÎÏ]/u' => 'I',
            '/[íìîï]/u' => 'i',
            '/[éèêë]/u' => 'e',
            '/[ÉÈÊË]/u' => 'E',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u' => 'O',
            '/[úùûü]/u' => 'u',
            '/[ÚÙÛÜ]/u' => 'U',
            '/ç/' => 'c',
            '/Ç/' => 'C',
            '/ñ/' => 'n',
            '/Ñ/' => 'N',
            '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u' => ' ', // Literally a single quote
            '/[“”«»„]/u' => ' ', // Double quote
            '/ /' => ' ', // nonbreaking space (equiv. to 0x160)
        );

        $string = preg_replace(array_keys($utf8), array_values($utf8), $text);
//        $string = str_replace('porn', '***', strtolower($string));
//        $string = str_replace('porno', '***', strtolower($string));
//        $string = str_replace('sex', '', strtolower($string));
//        $string = str_replace('fcuk', '***', strtolower($string));
//        $string = str_replace('fuck', '***', strtolower($string));
//        $string = str_replace('bitch', '***', strtolower($string));
        $string = stripslashes($string);
        // $string = htmlspecialchars($string);
        $string = htmlspecialchars_decode($string, ENT_QUOTES);
        //  $string = ucfirst(ucwords($string));

        return preg_replace('/[[:^print:]]/', '', trim($string));
    }

    /**
     * clean variable string
     * 
     * @param type $text
     * @return type
     */
    public function cleanStr($text) {
        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u' => 'A',
            '/[ÍÌÎÏ]/u' => 'I',
            '/[íìîï]/u' => 'i',
            '/[éèêë]/u' => 'e',
            '/[ÉÈÊË]/u' => 'E',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u' => 'O',
            '/[úùûü]/u' => 'u',
            '/[ÚÙÛÜ]/u' => 'U',
            '/ç/' => 'c',
            '/Ç/' => 'C',
            '/ñ/' => 'n',
            '/Ñ/' => 'N',
            '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u' => ' ', // Literally a single quote
            '/[“”«»„]/u' => ' ', // Double quote
            '/ /' => ' ', // nonbreaking space (equiv. to 0x160)
        );

        $string = preg_replace(array_keys($utf8), array_values($utf8), $text);
        $string = str_replace("'", '', $string);
        $string = str_replace(" ", '', $string);
        $string = str_replace('porn', '', strtolower($string));
        $string = str_replace('porno', '', strtolower($string));
        $string = str_replace('sex', '', strtolower($string));
        $string = str_replace('fcuk', '', strtolower($string));
        $string = str_replace('fuck', '', strtolower($string));
        $string = str_replace('drop', '', strtolower($string));
        $string = str_replace('delete', '', strtolower($string));
        $string = str_replace('update', '', strtolower($string));
        $string = str_replace('alter', '', strtolower($string));
        $string = stripslashes($string);
        // $string = htmlspecialchars($string);
        $string = strtoupper($string);

        return preg_replace('/[[:^print:]]/', '', trim($string));
    }

    /**
     * clean variable string
     * 
     * @param type $text
     * @return type
     */
    public function cleanString($text) {
        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u' => 'A',
            '/[ÍÌÎÏ]/u' => 'I',
            '/[íìîï]/u' => 'i',
            '/[éèêë]/u' => 'e',
            '/[ÉÈÊË]/u' => 'E',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u' => 'O',
            '/[úùûü]/u' => 'u',
            '/[ÚÙÛÜ]/u' => 'U',
            '/ç/' => 'c',
            '/Ç/' => 'C',
            '/ñ/' => 'n',
            '/Ñ/' => 'N',
            '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u' => ' ', // Literally a single quote
            '/[“”«»„]/u' => ' ', // Double quote
            '/ /' => ' ', // nonbreaking space (equiv. to 0x160)
        );
        $string = preg_replace(array_keys($utf8), array_values($utf8), $text);
        $string = stripslashes($string);
        // $string = htmlspecialchars($string);

        return preg_replace('/[[:^print:]]/', '', trim($string));
    }

    /**
     * SendPostAuthData
     * @param type $postUrl
     * @param type $postData
     */
    public function SendPostAuthData($postUrl, $postData, $authorisation) {

        $ch = curl_init($postUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Token ' . trim($authorisation),
            'Content-Length: ' . strlen(json_encode($postData))));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->settings['timeoutDuration']);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->settings['timeoutDuration']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->settings['appName'] . "/3.0");

        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curlError = curl_error($ch);
        curl_close($ch);

        return ["statusCode" => $status, "response" => $result, 'error' => $curlError];
    }

    /**
     * Send Post Data via cURL
     * @param type $postUrl
     * @param type $postData
     */
    public function sendPostData($postUrl, $postData) {
        $httpRequest = curl_init($postUrl);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, json_encode($postData));
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array('Content-Type: '
            . 'application/json', 'Content-Length: ' . strlen(json_encode($postData))));
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_TIMEOUT, $this->settings['timeoutDuration']);
        curl_setopt($httpRequest, CURLOPT_CONNECTTIMEOUT, $this->settings['timeoutDuration']);
        //accept SSL settings
        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($httpRequest, CURLOPT_USERPWD, 'stats@southwell.io:' . md5('stats@southwell.io'));
        curl_setopt($httpRequest, CURLOPT_USERAGENT, $this->settings['appName'] . "/3.0");
        $response = curl_exec($httpRequest);
        $status = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE);
        $curlError = curl_error($httpRequest);
        curl_close($httpRequest);

        return ["statusCode" => $status, "response" => $response, 'error' => $curlError];
    }

    /**
     * sendPostRequest
     * @param type $urlMeta
     * @param type $payload
     * @return type
     */
    public function sendPostRequest($urlMeta) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $urlMeta);
        curl_setopt($ch, CURLOPT_POST, 1);
        //  curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->settings['timeoutDuration']); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->settings['timeoutDuration']); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->settings['appName'] . "/3.0");
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return [
            "status" => isset($status) ? $status : 0,
            "response" => $result];
    }

    /**
     * sendGetRequest
     * @param type $url
     * @return type
     */
    public function sendGetRequest($url) {
        $httpRequest = curl_init($url);
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_USERAGENT, $this->settings['appName'] . "/3.0");
        $response = curl_exec($httpRequest);
        $status = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE);
        curl_close($httpRequest);

        return [
            'statusCode' => isset($status) ? $status : 0,
            'response' => json_decode($response)
        ];
    }

    /**
     * getMicrotime
     * @return type
     */
    public function getMicrotime() {
        list ($msec, $sec) = explode(" ", microtime());
        return ((float) $msec + (float) $sec);
    }

    /**
     * Return the current Date and time in the standard format
     * @param string $format the format in which to return the date
     * @return string
     */
    public function now($format = 'Y-m-d H:i:s', $timestamp = null) {
        if ($timestamp == null) {
            $timestamp = time();
        }
        return date($format, $timestamp);
    }

    /**
     * Checks validity of the date
     * @param DateTime $futureDate
     * @param DateTime $startDate
     * @return type
     */
   


    /**
     * Gets the Mobile operator Network
     * @param type $MSISDN
     * @return type
     */
    public function getMobileNetwork($MSISDN) {
        $network = "";
        $countryCode = substr($MSISDN, 0, 3);
        $mnoCode = substr($MSISDN, 3, 2);
        switch ($countryCode) {
            case 254://Kenya
                switch ($mnoCode) {
                    case 70:
                    case 71:
                    case 72:
                    case 74:
                    case 79:
                        $network = 'SAFARICOM';
                        break;
                    case 73:
                    case 78:
                    case 75:
                        $network = 'AIRTEL-KE';
                        $countryCode = substr($MSISDN, 0, 6);
                        if ($countryCode == '254757' || $countryCode == '254758' || $countryCode == '254759') {
                            $network = 'SAFARICOM';
                        }
                        break;
                    case 77:
                        $network = 'TELKOM-KE';
                        break;
                    case 76:
                        $network = 'EQUITEL';
                        $countryCode = substr($MSISDN, 0, 6);
                        if ($countryCode == '254768' || $countryCode == '254762' || $countryCode == '254769') {
                            $network = 'SAFARICOM';
                        }
                        break;
                    default:
                        $network = 'UNKNOWN';
                        break;
                }
                break;
            case 255://Tanzania
                $network = 'VODACOM';
                break;
            case 256://Uganda
                $network = 'MTN-UGX';
                break;
            case 250://Rwanda
                $network = 'MTN-RWX';
                break;
            default:
                $network = 'UNKNOWN';
                break;
        }
        return $network;
    }

    /**
     * Creates NewAuthToken
     * @param type $payload
     * @return type
     */
    public function createNewAuthToken($payload, $token = null) {
        if ($token == null) {
            $token = "55abe029fdebae5e1d417e2ffb2a003a0cd8b54763051cef08bc55abe029";
        }

        $secretKey = base64_encode($token);
        $jwtToken = jwt::encode($payload, $secretKey, 'HS512');

        return $jwtToken;
    }

    /**
     * messageOnly
     * @param type $function
     * @param type $message
     * @return Response
     */
    public function messageOnly($function, $message) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(200, "SUCCESS - MESSAGE ONLY");

        $res = json_encode($message);
        $response->setContent($res);

        $this->getLogFile('debug')->addWarning("$function - SUCCESS:$res");

        return $response;
    }

    /**
     * formats validation error response messages 
     * @param type $function
     * @return Response
     */
    public function forbiddenAcess($function, $message, $data) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(403, "FORBIDDEN ACCESS");

        $res = new \stdClass();
        $res->code = "Error";
        $res->statusDescription = $message;
        $res->data = $data;

        $res = json_encode($res);
        $response->setContent($res);
        $this->getLogFile('debug')->addWarning("$function - FORBIDDEN ACCESS:$res");

        return $response;
    }

    /**
     * formats validation Success response messages
     * @param type $function
     * @param type $message
     * @param type $data
     * @return Response
     */
    public function successLarge($function, $message, $data, $iserror = null) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(200, "SUCCESS");


        $res = new \stdClass();
        $res->code = "Success";
        if ($iserror) {
            $res->code = "Error";
        }

        $res->statusDescription = $message;
        $res->data = $data;

        $res = json_encode($res);
        $response->setContent($res);
        $this->getLogFile('debug')->addInfo("$function - SUCCESS: Data sent back");

        return $response;
    }

    /**
     * formats validation Success response messages
     * @param type $function
     * @param type $message
     * @param type $data
     * @return Response
     */
    public function success($function, $message, $data, $iserror = null) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(200, "SUCCESS");


        $res = new \stdClass();
        $res->code = "Success";
        if ($iserror) {
            $res->code = "Error";
        }

        $res->statusDescription = $message;
        $res->data = $data;

        $res = json_encode($res);
        $response->setContent($res);
        $this->getLogFile('debug')->addInfo("$function - SUCCESS:$res");

        return $response;
    }

    /**
     * formats validation error response messages 
     * @param type $function
     * @return Response
     */
    public function unProcessable($function, $message = null) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(422, "UNPROCESSABLE ENTITY");

        $res = new \stdClass();
        $res->code = "Error";
        $res->statusDescription = is_null($message) ? "Mandatory fields required!!" : $message;
        //$res->data = $data;

        $res = json_encode($res);
        $response->setContent($res);
        $this->getLogFile('debug')->addError("$function - UNPROCESSABLE:$res");

        return $response;
    }

    /**
     * Formats data error response messages 
     * @param type $function
     * @param type $message
     * @param type $data
     * @return Response
     */
    public function dataError($function, $message, $data) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(421, "DATA ERROR");

        $res = new \stdClass();
        $res->code = "Error";
        $res->statusDescription = $message;
        $res->data = $data;

        $res = json_encode($res);
        $response->setContent($res);
        $this->getLogFile('debug')->debug("$function - DATA ERROR:$res");

        return $response;
    }

    /**
     * Formats server error response messages 
     * @param type $function
     * @param type $message
     * @param type $data
     * @return Response
     */
    public function serverError($function, $message) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(500, "INTERNAL SERVER ERROR");

        $error = new \stdClass();
        $error->code = "Error";
        $error->statusDescription = $message;
        //$error->data = $data;

        $res = json_encode($error);
        $response->setContent($res);
        $this->getLogFile('debug')->addEmergency("$function - INTERNAL SERVER ERROR:$res");

        return $response;
    }

    /**
     * formats validation error response messages 
     * @param type $function
     * @return Response
     */
    public function unAuthorised($function, $message) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(401, "UN-AUTHORISED ACCESS");

        $res = new \stdClass();
        $res->code = "Error";
        $res->statusDescription = $message;
        //$res->data = $data;

        $res = json_encode($res);
        $response->setContent($res);
        $this->getLogFile('debug')->addWarning("$function - UN-AUTHORISED::$res");

        return $response;
    }

    /**
     * formats validation error response messages 
     * @param type $function
     * @return Response
     */
    public function MethodNotAllowed($function, $message) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(405, $message);

        $res = new \stdClass();
        $res->code = "Error";
        $res->statusDescription = $message;
        //$res->data = $data;

        $res = json_encode($res);
        $response->setContent($res);
        $this->getLogFile('debug')->addWarning("$function - METHOD NOT ALLOWED:$res");

        return $response;
    }

    /**
     * formats validation error response messages 
     * @param type $function
     * @return Response
     */
    public function PaymentRequired($function, $message) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(402, "PAYMENT REQUIRED");

        $res = new \stdClass();
        $res->code = "Error";
        $res->statusDescription = $message;
        //$res->data = $data;

        $res = json_encode($res);
        $response->setContent($res);
        $this->getLogFile('debug')->addNotice("$function - PAYMENT REQUIRED:$res");

        return $response;
    }

    /**
     * formats validation error response messages 
     * @param type $function
     * @return Response
     */
    public function BadRequest($function, $message, $data = null) {
        $response = new Response();
        $response->setHeader("Content-Type", "application/json");
        $response->setHeader("Access-Control-Allow-Origin", "*");
        $response->setStatusCode(400, "BAD REQUEST");

        $res = new \stdClass();
        $res->code = "Error";
        $res->statusDescription = $message;
        if ($data != null) {
            $res->data = $data;
        }

        $res = json_encode($res);
        $response->setContent($res);
        $this->getLogFile('debug')->addNotice("$function - BAD REQUEST:$res");

        return $response;
    }

}
