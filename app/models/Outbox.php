<?php

class Outbox extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $status;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var integer
     */
    public $message_id;

    /**
     *
     * @var integer
     */
    public $profile_id;

    /**
     *
     * @var integer
     */
    public $sp_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("smsservice");
        $this->setSource("outbox");
        $this->hasMany('id', 'Application\Models\OutboxDlr', 'outbox_id', ['alias' => 'OutboxDlr']);
        $this->belongsTo('sp_id', 'Application\Models\Serviceprovider', 'sp_id', ['alias' => 'Serviceprovider']);
        $this->belongsTo('message_id', 'Application\Models\Message', 'message_id', ['alias' => 'Message']);
        $this->belongsTo('profile_id', 'Application\Models\Profile', 'profile_id', ['alias' => 'Profile']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'outbox';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Outbox[]|Outbox|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Outbox|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
