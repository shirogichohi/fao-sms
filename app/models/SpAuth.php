<?php


class SpAuth extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $sp_autn_id;

    /**
     *
     * @var string
     */
    public $api_key;

    /**
     *
     * @var string
     */
    public $auth;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var integer
     */
    public $sp_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("smsservice");
        $this->setSource("sp_auth");
        $this->belongsTo('sp_id', 'Application\Models\Serviceprovider', 'sp_id', ['alias' => 'Serviceprovider']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'sp_auth';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SpAuth[]|SpAuth|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SpAuth|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
