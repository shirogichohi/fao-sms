<?php


class OutboxDlr extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $dlr_id;

    /**
     *
     * @var string
     */
    public $status;

    /**
     *
     * @var string
     */
    public $meta_data;

    /**
     *
     * @var string
     */
    public $delivered_at;

    /**
     *
     * @var integer
     */
    public $outbox_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("smsservice");
        $this->setSource("outbox_dlr");
        $this->belongsTo('outbox_id', 'Application\Models\Outbox', 'id', ['alias' => 'Outbox']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'outbox_dlr';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return OutboxDlr[]|OutboxDlr|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return OutboxDlr|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
