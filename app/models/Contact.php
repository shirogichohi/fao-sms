<?php


class Contact extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $contact_id;

    /**
     *
     * @var string
     */
    public $fullname;

    /**
     *
     * @var integer
     */
    public $campaign_id;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var integer
     */
    public $profile_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("smsservice");
        $this->setSource("contact");
        $this->belongsTo('profile_id', 'Application\Models\Profile', 'profile_id', ['alias' => 'Profile']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'contact';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Contact[]|Contact|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Contact|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
