<?php

class Serviceprovider extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $sp_id;

    /**
     *
     * @var string
     */
    public $sp_name;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("smsservice");
        $this->setSource("serviceprovider");
        $this->hasMany('sp_id', 'Application\Models\SpAuth', 'sp_id', ['alias' => 'SpAuth']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'serviceprovider';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Serviceprovider[]|Serviceprovider|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Serviceprovider|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
