<?php

use Phalcon\Mvc\Controller;
use ControllerBase as base;
use Carbon\Carbon;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class Users extends Controller {

    /**
     * CreatePaymentUser
     * @param type $payload
     */
    public static function CreatePaymentUser($payload) {
        $base = new base();

        try {
            try {
                return true;
            } catch (Exception $ex) {
                throw $ex;
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * LimmitedAccessControl
     * @return boolean
     */
    public static function AccessControlCheck($permissions) {
        $base = new base();

        try {
            $selectSql = "SELECT GROUP_CONCAT(`permission_id` SEPARATOR ':') acl "
                    . "FROM `user_permission` WHERE `permission_id` IN ($permissions)";
            $data = $base->rawSelect($selectSql);
            return isset($data[0]['acl']) ? $data[0]['acl'] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());

            return false;
        }
    }

    /**
     * MakerAccessControl
     * @return boolean
     */
    public static function MakerAccessControl() {
        $base = new base();

        try {
            $selectSql = "select * from user_permission where module_id in (1,2,4,5)"
                    . " and permission_id in(7,8,10,15,16,17,22,39,29,28,30);";
            $data = $base->rawSelect($selectSql);
            return isset($data[0]['acl']) ? $data[0]['acl'] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());

            return false;
        }
    }

    /**
     * CheckerAccessControl
     * @return boolean
     */
    public static function CheckerAccessControl() {
        $base = new base();

        try {
            $selectSql = "select GROUP_CONCAT(`permission_id` SEPARATOR ':') acl "
                    . "from user_permission where module_id in (1,2,4,5) and "
                    . "permission_id in(36,7,8,10,11,12,15,16,17,21,37,38,39,29,28,30)";
            $data = $base->rawSelect($selectSql);
            return isset($data[0]['acl']) ? $data[0]['acl'] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());

            return false;
        }
    }

    /**
     * LimmitedAccessControl
     * @return boolean
     */
    public static function LimmitedAccessControl() {
        $base = new base();

        try {
            $selectSql = "SELECT GROUP_CONCAT(`permission_id` SEPARATOR ':') acl "
                    . "FROM `user_permission` WHERE `module_id` IN (2,5) AND "
                    . "`permission_id` NOT IN (11,21,22,31,36,37,38,40)";
            $data = $base->rawSelect($selectSql);
            return isset($data[0]['acl']) ? $data[0]['acl'] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());

            return false;
        }
    }

    /**
     * UserPermissions
     * @return boolean
     */
    public static function UserPermissions() {
        $base = new base();

        try {
            $deniedAcl = $base->settings['Account']['ProtectedPermissions'];
            $selectSql = "SELECT GROUP_CONCAT(`permission_id` SEPARATOR ':') acl "
                    . "FROM `user_permission` WHERE permission_id NOT IN ($deniedAcl)";

            $data = $base->rawSelect($selectSql);
            $base->getLogFile('info')->emergency(__LINE__ . ":" . __CLASS__
                    . " | rawSelect::" . json_encode($data[0]['acl']));
            return isset($data[0]['acl']) ? $data[0]['acl'] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());

            return false;
        }
    }

    /**
     * GetClientUsingClientId
     * @param type $client_id
     */
    public static function GetClientUsingClientId($client_id) {
        $base = new base();

        try {
            $selectSql = "SELECT * FROM clients WHERE client_id=$client_id";

            $data = $base->rawSelect($selectSql);

            return isset($data[0]) ? $data[0] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());

            return false;
        }
    }

    /**
     * GetUserclientMapUsingClientId
     * @param type $client_id
     */
    public static function GetUserclientMapUsingClientId($client_id) {
        $base = new base();

        try {
            $selectSql = "SELECT user_client_map.user_mapId,user_client_map.api_token"
                    . ",user.user_id,user.email_address"
                    . ",user.full_names,clients.client_name,user_client_map.billing_modeId"
                    . ",user_client_map.status"
                    . ",client_balance.id account_id,client_balance.balance FROM clients"
                    . " INNER JOIN user_client_map ON clients.client_id=user_client_map.client_id"
                    . " JOIN client_balance ON user_client_map.client_id=client_balance.client_id"
                    . " JOIN user ON user_client_map.user_id=user.user_id WHERE"
                    . " user_client_map.client_id=:client_id LIMIT 1";

            $data = $base->rawSelect($selectSql, [':client_id' => $client_id]);

            return isset($data[0]) ? $data[0] : false;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * CreditClientUserAccount
     * @param type $params
     * @throws Exception
     */
    public static function CreditClientUserAccount($params) {
        $base = new base();

        try {
            $update = $base->rawUpdate('UPDATE `client_balance` SET'
                    . ' `balance`=`balance`+' . abs($params['amount'])
                    . ' WHERE `client_id`=' . $params['client_id'] . ' LIMIT 1');

            if ($update > 0) {
                $insert_trxn_sql = 'INSERT INTO `transactions`( `user_mapId`'
                        . ', `amount`, `reference_id`, `reference_type_id`, `source`'
                        . ', `description`, `created_at`) '
                        . 'VALUES (:user_mapId,:amount,:reference_id,:reference_type_id'
                        . ',:source,:description,NOW());';

                $insert_trx_params = [
                    ':amount' => abs($params['amount']),
                    ':source' => "CREDIT_WALLET",
                    ':user_mapId' => $params['user_mapId'],
                    ':description' => "Wallet Top Up Via Mpesa reference:" . $params['receipt'],
                    ':reference_type_id' => $base->settings['referenceTypeId']['WalletCredit'],
                    ':reference_id' => $params['reference_id']];

                $base->getLogFile("info")->addInfo(__LINE__ . ":" . __CLASS__
                        . " | <<<>>>>" . json_encode($insert_trx_params));

                return $transaction_id = $base->rawInsert($insert_trxn_sql, $insert_trx_params);
            }

            return 0;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * QueryUserUsingMobileNumber
     * @param type $mobile
     * @return boolean
     */
    public static function QueryUserUsingMobileNumber($mobile) {
        $base = new base();

        try {
            $selectSql = "SELECT user_client_map.user_mapId,user_client_map.client_id"
                    . ",clients.client_name,client_balance.id as account_id"
                    . ",clients.client_email,user_client_map.user_id,profile.profile_id "
                    . ",profile.msisdn,profile.network,user.email_address,user.full_names"
                    . ",user_client_map.billing_modeId,user_client_map.status,user_client_map.created_at "
                    . "FROM user_client_map INNER JOIN clients ON user_client_map.client_id=clients.client_id "
                    . "JOIN user ON user_client_map.user_id=user.user_id "
                    . "JOIN profile ON user.profile_id=profile.profile_id "
                    . "JOIN client_balance ON clients.client_id=client_balance.client_id"
                    . " WHERE profile.msisdn=:msisdn";
            $user = $base->rawSelect($selectSql, [':msisdn' => $mobile]);

            return isset($user[0]) ? $user[0] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());

            return false;
        }
    }

    /**
     * QueryUserUsingUserMapId
     * @param type $userMapId
     * @return array
     */
    public static function QueryUserUsingUserMapId($userMapId) {
        $base = new base();

        try {
            $selectSql = "SELECT user_client_map.user_mapId,user_client_map.client_id"
                    . ",clients.client_name,clients.client_email,user_client_map.user_id"
                    . ",profile.msisdn,profile.network,user.email_address,user.full_names"
                    . ",user_client_map.billing_modeId,user_client_map.status,user_client_map.created_at "
                    . "FROM user_client_map INNER JOIN clients ON user_client_map.client_id=clients.client_id "
                    . "JOIN user ON user_client_map.user_id=user.user_id "
                    . "JOIN profile ON user.profile_id=profile.profile_id "
                    . "WHERE user_client_map.user_mapId=:user_mapId "
                    . "AND user_client_map.status=:status";
            $user = $base->rawSelect($selectSql, [':status' => 1, ':user_mapId' => $userMapId]);

            return isset($user[0]) ? $user[0] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());

            return false;
        }
    }

    /**
     * QueryUserUsingUserId
     * @param type $userId
     * @return array
     */
    public function QueryUserUsingUserId($userId) {
        $base = new base();

        try {
            $selectSql = "SELECT user.`user_id`, user_client_map.user_mapId "
                    . ",user_client_map.client_id ,user_client_map.billing_modeId"
                    . ",user_client_map.permission_acl,user_client_map.api_token"
                    . ",user_client_map.status as loginStatus,clients.client_name,clients.client_email"
                    . ",profile.msisdn,profile.status,profile.network,user.`email_address`"
                    . ",user.`full_names`,user.`password`, user.`last_login`,user.`created_at`"
                    . " FROM `user` JOIN profile ON user.profile_id=profile.profile_id "
                    . "JOIN user_client_map ON user.user_id=user_client_map.user_id "
                    . "JOIN clients ON user_client_map.client_id=clients.client_id "
                    . "WHERE user.user_id=$userId";
            $result = $base->rawSelect($selectSql);

            return isset($result[0]) ? $result[0] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | QueryUserUsingUserId Exception::" . $ex->getMessage());
        }

        return false;
    }

    /**
     * QueryProfile
     * @param type $client_id
     * @return boolean
     */
    public function QueryProfile($client_id) {
        $base = new base();
        try {
            $selectSql = "SELECT ub.id account_id,u.profile_id, u.email_address"
                    . ", u.full_names, um.api_token,um.permission_acl, p.msisdn"
                    . ", ub.balance,ub.bonus,cs.unit_cost,cs.discount,um.user_mapId "
                    . "FROM user u INNER JOIN user_client_map um "
                    . "ON u.user_id = um.user_id JOIN profile p "
                    . "ON u.profile_id = p.profile_id JOIN client_balance ub "
                    . "ON  um.client_id= ub.client_id JOIN client_services cs "
                    . "ON cs.client_id=um.client_id WHERE "
                    . "um.client_id =:client_id AND cs.service_id=2";
            $result = $base->rawSelect($selectSql, [':client_id' => $client_id]);
            return isset($result[0]) ? $result[0] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | QueryProfile Exception::" . $ex->getMessage());
        }

        return false;
    }

    /**
     * QueryVerificationCode
     * @param type $v_code
     * @param type $user_id
     */
    public function QueryVerificationCode($v_code, $user_id) {
        $base = new base();
        try {
            $selectSql = "SELECT user_login.`user_login_id` FROM "
                    . "`user_login` WHERE "
                    . "user_login.`verification_code`=:verification_code "
                    . "AND user_login.`user_id`=:user_id";
            $result = $base->rawSelect($selectSql, [':verification_code' => md5($v_code)
                , ':user_id' => $user_id]);
            return isset($result[0]) ? $result[0] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | QueryVerificationCode Exception::" . $ex->getMessage());
        }

        return false;
    }

    /**
     * QueryUserUsingMobileOrEmail
     * @param type $token
     * @return \stdClass
     */
    public function QueryUserUsingUsername($username) {
        $base = new base();
        try {
            $selectSql = "SELECT user.`user_id`,profile.phone,profile.network,user.`password`"
                    . ",user.`created_at`, user.token FROM `user` "
                    . "JOIN profile ON user.profile_id=profile.profile_id "
                    . "WHERE user.username=:username ";
            $result = $base->rawSelect($selectSql, [':username' => $username]);

            return isset($result[0]) ? $result[0] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | QueryUserUsingMobileOrEmail Exception::" . $ex->getMessage());
        }

        return false;
    }

    /**
     * logFailedLoginAttempt
     * @param type $user_id
     * @param type $status
     * @return boolean
     */
    public function logLoginAttempt($user_id, $status = null) {
        $base = new base();
        $state = false;

        try {
            $query = "";
            if ($status == null) {
                $query = 'failed_attempts=failed_attempts+1'
                        . ',cumlative_failed_attempts=cumlative_failed_attempts+1'
                        . ',last_failed_attempt=NOW() ';
            }

            if ($status == 1) {
                $query = 'successful_attempts=successful_attempts+1'
                        . ',last_successful_date=NOW()';
            }

            if ($status == 2) {
                $query = 'successful_attempts=successful_attempts+1'
                        . ',last_successful_date=NOW()'
                        . ',failed_attempts=0';
            }

            $insert_sql = "UPDATE `user_login` SET $query "
                    . "WHERE user_id=:user_id LIMIT 1";
            $insert_params = [
                ":user_id" => $user_id,];

            $res = $base->rawInsert($insert_sql, $insert_params);
            if ($res > 0) {
                return true;
            }
        } catch (Exception $ex) {
            $base->getLogFile('error')->addError(__LINE__ . ":" . __CLASS__
                    . " | Exception >>> " . json_encode($ex->getCode()));
        }

        return $state;
    }

    /**
     *  check if user passes login rules
     * @param type $user_id
     * @return boolean|int
     */
    public function canLogin($user_id) {
        $base = new base();

        $sql = "SELECT user_login_id,failed_attempts,IFNULL(last_failed_attempt,0) "
                . "AS last_failed_attempt FROM user_login WHERE user_id = $user_id ";
        try {
            $login = $base->rawSelect($sql);
            if (!empty($login) || count($login) > 0) {
                $user_login_id = $login[0]['user_login_id'];
                $failed_attempts = $login[0]['failed_attempts'];
                $last_failed_attempt = isset($login[0]['last_failed_attempt']) ?
                        $login[0]['last_failed_attempt'] : 0;

                if ($failed_attempts < 5 || $last_failed_attempt == 0) {
                    $this->logLoginAttempt($user_id, 2);

                    return 1;
                } else {
                    $last_failed = Carbon::createFromFormat('Y-m-d H:i:s', $last_failed_attempt);
                    $now = Carbon::now();
                    $minutes = $last_failed->diffInMinutes($now);
                    $interval = $this->settings['Authentication']['failedAttemptsInterval'];

                    if ($minutes > $interval) {
                        $this->logLoginAttempt($user_id, 2);

                        return 1;
                    } else {
                        return false;
                    }
                }

                $this->logLoginAttempt($user_id, 1);

                return 1;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            $base->getLogFile('error')->addError(__LINE__ . ":" . __CLASS__
                    . " | Exception >>> " . json_encode($ex->getCode()));

            return false;
        }

        return 1;
    }

    /**
     * resetPassword
     * @param type $user_id
     * @param type $new_password
     * @param type $verification_code
     * @return boolean
     */
    public function resetPassword($user_id, $new_password, $v_code) {
        $base = new base();

        try {
            $password = $this->security->hash(md5($new_password));
            $token_api = md5("$password$v_code" . date('yyyyMMddHHmmss'));
            $verification_code = md5($v_code);

            $updateLoginSql = "UPDATE `user_login` "
                    . "SET `verification_code`='$verification_code' "
                    . "WHERE `user_id`=$user_id LIMIT 1";
            if ($base->rawUpdate($updateLoginSql) > 0) {
                $updatePassSql = "UPDATE `user` "
                        . "SET `password`='$password' "
                        . "WHERE `user_id`=$user_id LIMIT 1";
                if ($base->rawUpdate($updatePassSql) > 0) {
                    $updateTokenSql = "UPDATE `user_client_map` SET `api_token`='$token_api' "
                            . "WHERE `user_id`=$user_id LIMIT 1";
                    if ($base->rawUpdate($updateTokenSql) > 0) {
                        return TRUE;
                    }
                }
                return FALSE;
            }

            return false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | resetPassword Exception::" . $ex->getMessage());
            return false;
        }
    }

    /**
     * QueryWallet
     * @param type $account_id
     */
    public static function QueryWallet($account_id) {
        $base = new base();

        try {
            $selectSql = "SELECT client_balance.id account_id,client_balance.balance,"
                    . "client_balance.client_id FROM client_balance WHERE "
                    . "client_balance.id=:id LIMIT 1";

            $data = $base->rawSelect($selectSql, [':id' => $account_id]);

            return isset($data[0]) ? $data[0] : false;
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());

            return false;
        }
    }

}
