<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

$connection = [];
$logPath = [];
$CoreUrl = "";

$host = gethostname();

    $connection = array(
        'adapter' => 'mysql',
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => '',
        'dbname' => 'smsservice',
        'charset' => 'utf8'
    );

    $connection2 = [
        'adapter' => 'mysql',
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => '',
        'dbname' => 'smsservice',
        'charset' => 'utf8',
        "options" => [\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true]
    ];

    $logPath = [
        'location' => "logs/",
        "dateFormat" => "Y-m-d H:i:s",
        "output" => "%datetime% - [%level_name%] - %message%\n",
        "systemName" => "smsservice"
    ];

    $CoreUrl = "";


return new \Phalcon\Config([
    'database' => $connection,
    'db2' => $connection2,
    'application' => [
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir' => APP_PATH . '/app/models/',
        'cacheDir' => APP_PATH . '/app/cache/',
        'baseUri' => '/m_gifts/',],
    'logPath' => $logPath,
#RabbitMQ settings
    'mQueue' => [
        'rabbitServer' => "",
        'rabbitVHost' => "/",
        'rabbitUser' => "",
        'rabbitPass' => "",
        'rabbitPortNo' => "",
        'QueueName' => "_QUEUE",
        'QueueExchange' => "_EXCHANGE",
        'QueueRoute' => "_ROUTE",]

        ]);
