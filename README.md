Welcome to Fao SMS

---

Description

This service is designed and built to send bulk SMS. The messages can be grouped into campaigns
where one, two or more of them are sent to multiple recipients.
Recipients have a contact database where they can be in one or more campaigns

---

This repo is Phalcon Frame work

The Folder APP

---

1. Config Folder That consit of

config.php File that for dbconnection

2. Controllers

   Controller Base - It the primary controller that is extended on all controllers
   SenderSMS controller - Use to send and view SMS
   Servicesprovider controller -- Used to add services and view provider
   User Controller -- use to manage all used to the system

3. Models

Consist of all tables that are used. They include campaign contact, message, outbox, outbox_dlr, profile, role, services_provider, sp_auth, user

4. Utils

It consist of two classes that include authenticate and user

---

Database Folder

Consisit of mysql database smsservices.sql

---

Logs

Use for recording all logs that are group into Debug, Services Error and Lastly Info Logs

---

Index.php

Display routes for API in Post and Get Method

---

API Unit Tests

Used Postman, and can be accessed via https://documenter.getpostman.com/view/8924043/SVn2Mv9V?version=latest

---
