<?php

use Phalcon\Mvc\Micro;
use Phalcon\Loader;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Http\Response;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Mvc\Micro\Exception;
use Phalcon\Events\Manager;
use FirewallMiddleware as firewall;

error_reporting(E_ALL);
define('APP_PATH', realpath(''));

ini_set("date.timezone", "Africa/Nairobi");

/**
 * Read auto-loader
 */
include APP_PATH . "/vendor/autoload.php";


/**
 * Read the configuration
 */
$config = include APP_PATH . "/app/config/config.php";

/**
 * Read auto-loader
 */
include APP_PATH . "/app/config/loader.php";

/**
 * Read services
 */
include APP_PATH . "/app/config/services.php";

/**
 * Create a new Events Manager.
 */
//$eventsManager = new Manager();

/**
 * create and bind the DI to the application
 */
$app = new Micro($di);


/**
 * user Signup
 */
$user = new MicroCollection();
$user->setPrefix('/v2/users/');
$user->setHandler('UserController', true);
$user->post('create', 'createUsers');
$user->get('create', 'createUsers');
$user->post('login', 'loginAction');
$user->get('login', 'loginAction');

/**
 * Contacts
 */
$contact = new MicroCollection();
$contact->setPrefix('/v2/contact/');
$contact->setHandler('ContactController', true);
$contact->post('create/campaign', 'createCampaign');
$contact->get('create/campaign', 'createCampaign');
$contact->post('view/campaign', 'viewCampaign');
$contact->get('view/campaign', 'viewCampaign');
$contact->post('create/contact', 'createContact');
$contact->get('create/contact', 'createContact');
$contact->post('view/contact', 'viewContact');
$contact->get('view/contact', 'viewContact');

/**
 * servicesProvider
 */
$spa = new MicroCollection();
$spa->setPrefix('/v2/provider/');
$spa->setHandler('ServiceproviderController', true);
$spa->post('create/spa', 'createServiceprovider');
$spa->get('create/spa', 'createServiceprovider');
$spa->post('view/spa', 'viewServicesProvider');
$spa->get('view/spa', 'viewServicesProvider');

/**
 * SMS
 */
$sms = new MicroCollection();
$sms->setPrefix('/v2/sms/');
$sms->setHandler('sendSMSController', true);
$sms->post('send', 'sendSMSSingle');
$sms->get('send', 'sendSMSSingle');
$sms->post('view', 'viewSMS');
$sms->get('view', 'viewSMS');



/**
 * mount points
 */
$app->mount($user);
$app->mount($contact);
$app->mount($spa);
$app->mount($sms);

try {
    /**
     * Firewall middleware
     */
    $app->before(function () use($app) {
        
    });

    /**
     * Not Found URLs
     */
    $app->notFound(function () use ($app) {
        $app->response->setStatusCode(404, "Not Found")->sendHeaders();
        $exception = 'URI Not Found:  ' . $app->request->getMethod() . ' ' . $app->request->getURI();

        $res = new \stdClass();
        $res->code = "Error";
        $res->statusDescription = $exception;

        $app->response->setContent(json_encode($res));
    });

    // Handle the request
    $response = $app->handle();
} catch (\Exception $e) {
    $res = new \stdClass();
    $res->code = "Error";
    $res->message = "An Error Ocurred. Reason:" . $e->getMessage();
    $res->data = [];

    header("Content-Type: application/json;charset=utf-8");
    header('Access-Control-Allow-Origin:*');

    $phpSapiName = substr(php_sapi_name(), 0, 3);
    if ($phpSapiName == 'cgi' || $phpSapiName == 'fpm') {
        http_response_code(500);
    } else {
        $protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0    ';
        http_response_code(500);
    }

    echo json_encode($res);
}
