<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));

$connection = [];
$logPath = [];
$CoreUrl = "";

$host = gethostname();
$hosts = ['ke-pr-web-1', 'ke-pr-core-1'];
if (!in_array($host, $hosts)) {
    $connection = array(
        'adapter' => 'mysql',
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => '',
        'dbname' => 'tableBanking',
        'charset' => 'utf8'
    );

    $connection2 = [
        'adapter' => 'mysql',
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => '',
        'dbname' => 'tableBanking',
        'charset' => 'utf8',
        "options" => [\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true]
    ];

    $logPath = [
        'location' => "/var/www/log/tableBanking",
        "dateFormat" => "Y-m-d H:i:s",
        "output" => "%datetime% - [%level_name%] - %message%\n",
        "systemName" => "tableBanking"
    ];

    $CoreUrl = "";
} else {
    $connection = array(
        'adapter' => 'mysql',
        'host' => '10.132.0.3',
        'username' => 'app_user',
        'password' => 'e9mqCVfTErt1IATT3VmK1iOdtljsXPfV',
        'dbname' => 'tableBanking',
        'charset' => 'utf8'
    );

    $connection2 = array(
        'adapter' => 'mysql',
        'host' => '10.132.0.3',
        'username' => 'app_user',
        'password' => 'e9mqCVfTErt1IATT3VmK1iOdtljsXPfV',
        'dbname' => 'tableBanking',
        'charset' => 'utf8',
        "options" => array(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true)
    );

    $logPath = [
        'location' => "/var/www/logs/tableBanking/",
        "dateFormat" => "Y-m-d H:i:s",
        "output" => "%datetime% - [%level_name%] - %message%\n",
        "systemName" => "tableBanking"
    ];

    $CoreUrl = "";
}

return new \Phalcon\Config([
    'database' => $connection,
    'db2' => $connection2,
    'application' => [
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir' => APP_PATH . '/app/models/',
        'cacheDir' => APP_PATH . '/app/cache/',
        'baseUri' => '/m_gifts/',],
    'logPath' => $logPath,
#RabbitMQ settings
    'mQueue' => [
        'rabbitServer' => "35.187.112.31",
        'rabbitVHost' => "/",
        'rabbitUser' => "southwell",
        'rabbitPass' => "southwell",
        'rabbitPortNo' => "5672",
        'QueueName' => "_QUEUE",
        'QueueExchange' => "_EXCHANGE",
        'QueueRoute' => "_ROUTE",],
    #settings
    'settings' => [
        'CustomerSupportLine' => '',
        'appName' => 'M-Predict',
        'defaultSenderId' => 'Mkasha',
        'defaultUssdCode' => '309',
        'SelectRecordLimit' => '20',
        'Account' => [
            'APInvocations' => 250,
            'NewSignUpsInterval' => 1,
            'InvoicePrefix' => 'INV',
            'SenderIdPrefix' => 'ISD',
            'Prefix' => 'ACC',
            'FreeBalance' => 0,
            'SmsUnitCost' => 0.8,
            'AckSmsUnitCost' => 0.8,
            'OtherSMSUnitCost' => 1.2,
            'SmsDiscount' => 0,
            'FreeBonus' => 10,
            'WalletAlertThrehold' => 1000,
            'Aggregation_TPS' => 6,
            'ProtectedPermissions' => "64,65",
            'LimitedPermissions' =>  "64,65",
            'defaultReferralID' => 299222
        ],
        
        'mnoApps' => [
            'ApiKey' => '578f564063375b096678d970812c3752',
            'outgoingUrl' => 'http://134.209.29.136/pro_api/v3/BulkSMS/api/create',
            'DefaultUssdMenu' => '',
        ],
        'Queues' => [
            'Outbox' => [
                'Queue' => 'tableBanking_QUICK',
                'Exchange' => 'tableBanking_QUICK',
                'Route' => 'tableBanking_QUICK'],
            'FailedAck' => [
                'Queue' => 'tableBanking_ACK',
                'Exchange' => 'tableBanking_ACK',
                'Route' => 'tableBanking_ACK'],],
        'Mpesa' => [
            'DefaultAccountRef' => 'PREDICT',
            'DefaultC2BPaybill' => '',
        ],
        'Authentication' => [
            'recommendedPasswordXters' => 6,
            'failedAttemptsInterval' => 10,],
        'connectTimeout' => 30,
        'timeoutDuration' => 30,
        'tokenKey' => '55abe029fdebae5e1d417e2ffb2a003a0cd8b54763051cef08bc55abe029',
        'MailSettings' => [
            'AdminSender' => '',
            'AdminPass' => '',
            'AdminName' => '',
            'WebMaster' => '',
            'SupportMaster' => '',
            'TechMaster' => '',
            'SalesMaster' => '',],
        'ReferenceTypeId' => [
            'WalletDebit' => 1,
            'WalletCredit' => 2,
            'WalletDeposit' => 3,
            'WalletStake' => 4,
            'WalletReversal' => 5,],
        'Messages' => [
            'SendTips' => '',
            'NewPlayer' => '{first_name}, Your MKasha Gifts account has been created By {agent_name}. Your OTP is {OTP}. {line}Dial {dial_code} to access your  account.',
            'SuccessfulTopUp' => '{first_name}, Your Payment of KES.{amount} has been recieved on {date}.{line}{agent_name} Thanks you.{line}Dial {dial_code} to access your account',],],
        ]);
