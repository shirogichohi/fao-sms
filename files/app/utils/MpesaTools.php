<?php

/**
 * Description of Invoicing
 *
 * @author User
 */
use Phalcon\Mvc\Controller;
use ControllerBase as base;

class MpesaTools extends Controller {

    /**
     * paybillSettings
     * Example
     * shortCode    passKey    
     * "880088"=>array("passKey")
     * @return type
     */
    public static function paybillSettings($paybill) {
        $configs = [
            "839270" => ["7e3b57de6e2c8fd823d08f2c8366f6b1dfde9342c0ff00c9f2303994b5a45ee1", "MKASHA SERVICES LIMITED", "SalaryPayment", "suz2ernGQtqvVzxE176bp8u6lYSo5kWw", "9RTn5TkprPCB562T", 'jnwangi'],
        ];

        return isset($configs[$paybill]) ? $configs[$paybill] : false;
    }

    /**
     * InitiateMpesaStk
     * @param type $amount
     * @param type $mobileNumber
     * @param type $paybillNumber
     * @param type $accountReference
     * @param type $callBackURL
     * @return type
     */
    public static function InitiateMpesaStk($amount
    , $mobileNumber
    , $paybillNumber = null
    , $accountReference = null
    , $callBackURL = null) {
        $base = new base();
        if ($accountReference == null) {
            $accountReference = $base->settings['Mpesa']['DefaultAccountRef'];
        }

        if ($paybillNumber == null) {
            $paybillNumber = $base->settings['Mpesa']['DefaultPaybill'];
        }

        if ($base->getMobileNetwork($mobileNumber) != 'SAFARICOM') {
            return ['code' => 401, 'message' => 'Invalid Mobile Network. Only Safaricom Allowed.'];
        }

        $paybillsMap = self::paybillSettings($paybillNumber);
        try {
            if (!$paybillsMap) {
                $base->getLogFile('error')->error(__LINE__
                        . ":" . __CLASS__ . " | M-pesa Paybill provided does NOT exist");
                return ['code' => 404, 'message' => 'M-pesa Paybill provided does NOT exist.'];
            }

            if (empty($paybillsMap[0])) {
                $base->getLogFile('error')->error(__LINE__
                        . ":" . __CLASS__ . " | M-pesa Paybill[$paybillNumber]"
                        . "Passkey Not provided");
                return ['code' => 401, 'message' => 'M-pesa Paybill [' . $paybillNumber
                    . '] Passkey Not provided'];
            }

            if (empty($paybillsMap[2])) {
                $base->getLogFile('error')->error(__LINE__
                        . ":" . __CLASS__ . " | M-pesa Paybill [$paybillNumber] "
                        . "Transaction Type NOT provided");
                return ['code' => 401, 'message' => "M-pesa Paybill [$paybillNumber] "
                    . "Transaction Type NOT provided"];
            }

            $payload['passKey'] = $paybillsMap[0];
            $payload['paybillClient'] = $paybillsMap[1];
            $payload['transactionType'] = $paybillsMap[2];
            $payload['app_key'] = $paybillsMap[3];
            $payload['app_secret'] = $paybillsMap[4];

            if ($callBackURL == NULL) {
                $callBackURL = $base->settings['Mpesa']['URLs']['CheckOutURL'];
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $base->settings['Mpesa']['URLs']['mpesaAuthUrl']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            $headers = array(
                'Authorization: Basic ' . base64_encode($payload['app_key'] . ":" . $payload['app_secret'])
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $errors = curl_error($ch);
            $result = json_decode(curl_exec($ch));
            $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $base->getLogFile('info')->info(__LINE__
                    . ":" . __CLASS__ . " | OAuth2 REQUEST::"
                    . json_encode(["statusCode" => $returnCode
                        , "response" => $result, "error" => $errors]));

            if ($returnCode != 200 && empty($result)) {
                $base->getLogFile('error')->error(__LINE__
                        . ":" . __CLASS__ . " | M-pesa Paybill [$paybillNumber] "
                        . "STK Checkout authentication Failed");

                return ['code' => 401, 'message' => "STK Checkout authentication Failed."];
            }

            $payload['token'] = $result->access_token;
            /**
             * Checkout process payment
             */
            $timestamp = date("YmdHis");
            $checkoutPayload = [
                "BusinessShortCode" => $paybillNumber,
                "Password" => base64_encode($paybillNumber . "" . $payload['passKey'] . "" . $timestamp),
                "Timestamp" => $timestamp,
                "TransactionType" => $payload['transactionType'],
                "Amount" => (int) intval($amount),
                "PartyA" => $mobileNumber,
                "PartyB" => $paybillNumber,
                "PhoneNumber" => $mobileNumber,
                "CallBackURL" => $callBackURL,
                "AccountReference" => $accountReference,
                "TransactionDesc" => $payload['transactionType']
            ];

            $httpRequest = curl_init();
            curl_setopt($httpRequest, CURLOPT_URL, $base->settings['Mpesa']['URLs']['mpesaResourceUrl']);
            curl_setopt($httpRequest, CURLOPT_NOBODY, true);
            curl_setopt($httpRequest, CURLOPT_POST, true);
            curl_setopt($httpRequest, CURLOPT_CONNECTTIMEOUT, 20);
            curl_setopt($httpRequest, CURLOPT_POSTFIELDS, json_encode($checkoutPayload));
            curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            $authHeaders = array(
                'Content-Type: application/json',
                'Authorization:Bearer ' . $payload['token'],
            );
            curl_setopt($httpRequest, CURLOPT_HTTPHEADER, $authHeaders);
            curl_setopt($httpRequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, false);
            $responseErrors = curl_error($httpRequest);
            $lmnoResponse = curl_exec($httpRequest);
            $statusCode = (int) curl_getinfo($httpRequest, CURLINFO_HTTP_CODE);
            curl_close($httpRequest);

            $base->getLogFile('info')->addInfo(__LINE__ . ":" . __CLASS__
                    . " | Lipa Na M-Pesa Online Payment API REQUEST::"
                    . json_encode(["statusCode" => $statusCode, "response" => json_decode($lmnoResponse)
                        , "error" => $responseErrors, 'payload' => $checkoutPayload]));

            $decodedResult = json_decode($lmnoResponse);
            if ($statusCode == 200) {//success
                return ['code' => 200, 'message' => 'Successfully Initiated STK. ' . $decodedResult->ResponseDescription
                    , 'data' => [ 'MerchantRequestID' => $decodedResult->MerchantRequestID,
                        'CheckoutRequestID' => $decodedResult->CheckoutRequestID]];
            } else {
                $requestId = (isset($decodedResult->requestId) ? $decodedResult->requestId : null);
                $errorCode = (isset($decodedResult->errorCode) ? $decodedResult->errorCode : null);
                $errorMessage = (isset($decodedResult->errorMessage) ? $decodedResult->errorMessage : null);

                return ['code' => $statusCode, 'message' => is_null($errorMessage) ? 'Mpesa Stk Request Failed' : $errorMessage
                    , 'data' => [
                        "requestId" => $requestId,
                        "errorCode" => $errorCode,
                ]];
            }
        } catch (Exception $ex) {
            $base->getLogFile('error')->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exceptions:" . $ex->getMessage());
            return ['code' => 500, 'message' => $ex->getMessage()];
        }
    }

    /**
     * InitiateB2CPayment
     * @param type $mobileNumber
     * @param type $b2cPaybillNumber
     * @param type $withdrawalAmount
     * @return array
     */
    public static function InitiateB2CPayment($mobileNumber, $b2cPaybillNumber
    , $withdrawalAmount, $unique_id, $Remarks = null, $occation = null) {
        $base = new base();

        if (!$Remarks) {
            $Remarks = "Business to Customer Payment";
        }

        if (!$occation) {
            $occation = "Jamuhuri";
        }

        if (!$unique_id || !is_numeric($unique_id)) {
            return ['code' => 422, 'message' => 'Transaction Unique Identifier is required!'];
        }

        $paybillsMap = self::paybillSettings($b2cPaybillNumber);
        if (!$paybillsMap) {
            return ['code' => 400, 'message' => 'Invalid Paybill Credentials Found. '
                . 'Contact System Administrator for assistance.'];
        }

        if (!$withdrawalAmount || !is_numeric($withdrawalAmount) || $withdrawalAmount < 1) {
            return ['code' => 422, 'message' => 'Invalid Amount Provided!'];
        }

        $authResponse = self
                ::Authentication($base->settings['Mpesa']['URLs']['mpesaAuthUrl']
                        , $paybillsMap[4]
                        , $paybillsMap[3]);
        $base->getLogFile('info')->info(__LINE__ . ":" . __CLASS__
                . " | $b2cPaybillNumber"
                . " | $mobileNumber"
                . " | Authentication:" . json_encode($authResponse));

        if ($authResponse['statusCode'] == 0) {
            return ['code' => 408, 'message' => 'Request Timed Out'];
        }

        if ($authResponse['statusCode'] == 401) {
            return ['code' => 401, 'message' => $authResponse['result']->errorMessage];
        }

        if ($authResponse['statusCode'] != 200) {
            return ['code' => $authResponse['statusCode'], 'message' => isset($authResponse['result']->errorMessage) ?
                        $authResponse['result']->errorMessage :
                        "An Error Occured."];
        }

        $SecurityCredential = self::B2CTokens($b2cPaybillNumber);
        if (!$SecurityCredential) {
            return ['code' => 401, 'message' => "Invalid Security Detail."];
        }

        $payload = [
            'InitiatorName' => $paybillsMap[count($paybillsMap) - 1],
            'SecurityCredential' => $SecurityCredential,
            'CommandID' => $paybillsMap[2],
            'Amount' => abs($withdrawalAmount),
            'PartyA' => $b2cPaybillNumber,
            'PartyB' => $mobileNumber,
            'Remarks' => $Remarks,
            'QueueTimeOutURL' => $base->settings['Mpesa']['URLs']['QueueTimeOutURL'],
            'ResultURL' => $base->settings['Mpesa']['URLs']['ResultURL'],
            'Occassion' => "$occation",];

        $httpRequest = curl_init();
        curl_setopt($httpRequest, CURLOPT_URL, $base->settings['Mpesa']['URLs']['B2CURL']);
        curl_setopt($httpRequest, CURLOPT_NOBODY, true);
        curl_setopt($httpRequest, CURLOPT_POST, true);
        curl_setopt($httpRequest, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($httpRequest, CURLOPT_POSTFIELDS, json_encode($payload));
        curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        $authHeaders = array(
            'Content-Type: application/json',
            'Authorization:Bearer ' . $authResponse['result']->access_token,);
        curl_setopt($httpRequest, CURLOPT_HTTPHEADER, $authHeaders);
        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($httpRequest, CURLOPT_SSL_VERIFYPEER, false);

        $lmnoResponse = curl_exec($httpRequest);
        $statusCode = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE);
        $responseErrors = curl_error($httpRequest);
        curl_close($httpRequest);

        $lmnoResponse = json_decode($lmnoResponse);

        $base->getLogFile('info')->info(__LINE__ . ":" . __CLASS__
                . " | $b2cPaybillNumber"
                . " | $mobileNumber"
                . " | Http Code:$statusCode"
                . " | Http Error:$responseErrors"
                . " | B2C Request:" . json_encode($lmnoResponse));

        if ($statusCode != 200) {
            return $response = ['code' => $statusCode,
                'message' => isset($lmnoResponse->errorMessage) ?
                        $lmnoResponse->errorMessage :
                        "An Error Occured."];
        }

        return ['code' => 200, 'message' => $lmnoResponse->ResponseDescription, 'data' => $lmnoResponse];
    }

    /**
     * InitiateB2BPayment
     * @param type $mobileNumber
     * @param type $b2cPaybillNumber
     * @param type $withdrawalAmount
     * @param type $c2bPaybill
     * @param type $Remarks
     * @param type $occation
     * @return type
     */
    public static function InitiateB2BPayment($mobileNumber, $b2cPaybillNumber
    , $withdrawalAmount, $c2bPaybill, $Remarks = null, $occation = null) {
        $base = new base();

        return ['code' => 200, 'message' => "", 'data' => []];
    }

    /**
     * Authentication
     * @param type $app_secret - application secret
     * @param type $app_key - application key
     * @return type
     */
    private static function Authentication($auth_url, $app_secret, $app_key) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $auth_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Basic ' . base64_encode($app_key . ":" . $app_secret)]);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = json_decode(curl_exec($ch));
        $errors = curl_error($ch);
        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return [
            'statusCode' => isset($returnCode) ? $returnCode : 0,
            'error' => $errors,
            'result' => $result,
        ];
    }

    /**
     * B2CTokens
     * @param type $paybill
     * @return type
     */
    protected static function B2CTokens($paybill) {
        $tokens = [
            '839270' => 'i2VEMGwGNEWYLtft6PLCeRs85RMOEl0a6Sg5TVILMl2uBLokB+JT3pcSukl/3JX9W5EUc//eW8EGAY3YXabOZnSdOs7yFhqZQfE1rXU7Wof2K/wvn1URC2cBKZ/SG+u9/NBfzp2uFcYrKphcdYrNKmFwHZcIhkku0Uqa9NwQSsfTA9M5gfenb+V09scd7sBO5yIFx1C7tQKWKWfzHRdh1WhSseLU/8DzIttfF2IgcAQ08QytGCKShUQ7IcZElfqnX0K+5oB3l5IaDdYSXkiu1Dvt+NLpBnLQRl5BBwGyxZhPP1lxU5i+bPquT8QKzgF3uimeFBAelnG3PHt2I0yCNw==',
        ];

        return isset($tokens[$paybill]) ? $tokens[$paybill] : false;
    }

}
