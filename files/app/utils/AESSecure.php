<?php

/**
 * Description of AESSecure
 *
 * @author User
 */
use Phalcon\Mvc\Controller;
use \Firebase\JWT\JWT;
use ControllerBase as base;

class AESSecure extends Controller {

    /**
     * EncryptData
     * @param type $password
     * @param type $data
     * @return type
     * @throws Exception
     */
    public static function EncryptData($password, $data) {
        try {
            $method = 'aes-256-cbc';
            /**
             * Must be exact 32 chars (256 bit)
             * You must store this secret random key in a safe place of your system.
             */
            if (!is_string($data)) {
                $data = json_encode($data);
            }

            $key = substr(hash('sha256', $password, true), 0, 32);

            /**
             *  IV must be exact 16 chars (128 bit)
             */
            $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

            return base64_encode(openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv));
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * DecryptData
     * @param type $password
     * @param type $data
     * @return type
     * @throws Exception
     */
    public static function DecryptData($password, $data) {
        try {
            $method = 'aes-256-cbc';
            /**
             * Must be exact 32 chars (256 bit)
             * You must store this secret random key in a safe place of your system.
             */
            if (!is_string($data)) {
                $data = json_encode($data);
            }

            $key = substr(hash('sha256', $password, true), 0, 32);

            /**
             *  IV must be exact 16 chars (128 bit)
             */
            $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

            return openssl_decrypt(base64_decode($data), $method, $key, OPENSSL_RAW_DATA, $iv);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * decodeAuthToken
     * @param type $token - jwt token
     * @param type $service - Service to check
     */
    public function decodeAuthToken($token, $service) {
        $base = new base();
        try {
            $secretKey = base64_encode($base->settings['tokenKey']);
            $data_arr = JWT::decode($token, $secretKey, array('HS512'));
            $data = (array) $data_arr;

            $base->getLogFile()->addInfo(__LINE__ . ":" . __CLASS__ . " - | "
                    . "Decoded Data::" . json_encode($data));

            if (in_array($data['server'], [gethostname(), 'ke-pr-web-1', 'ke-pr-core-1'])) {
                if ($data['service'] == $service) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        } catch (\Exception $ex) {
            $base->getLogFile()->emergency(__LINE__ . ":" . __CLASS__ . " - | decodeAuthToken "
                    . "Failed: Reason-" . $ex->getMessage());

            return false;
        }
    }

}
