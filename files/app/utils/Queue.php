<?php

/**
 * Description of Queue
 *
 * @author User
 */
use Phalcon\Mvc\Controller;
use ControllerBase as base;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Queue extends Controller {

    /**
     * ConnectAndPublishToQueue
     * @param type $payload
     * @param type $queueName
     * @param type $exchangeKey
     * @param type $routeKey
     * @param type $server
     * @param type $port
     * @param type $user
     * @param type $pass
     * @param type $vhost
     * @return \stdClass
     */
    public function ConnectAndPublishToQueue($payload, $queueName, $exchangeKey, $routeKey
    , $server = null, $port = null, $user = null, $pass = null, $vhost = "/") {
        $response = new \stdClass();
        $base = new base();
        $start = $base->getMicrotime();

        if (!$queueName || !$exchangeKey || !$routeKey) {
            $response->code = 422;
            $response->statusDescription = "Mandatory Fields are missing!!";
            $response->data = [];

            return $response;
        }

        $queueName = strtoupper($queueName) . '_QUEUE'; //queue name
        $exchangeKey = strtoupper($exchangeKey) . '_EXCHANGE'; //queue exchange,
        $routeKey = strtoupper($routeKey) . '_ROUTE'; //queue routing

        if (!$vhost) {
            $vhost = "/";
        }

        $conn = $this->createRabbitConnection($server, $port, $user, $pass, $vhost);

        if ($conn === null) {
            $response->code = 401;
            $response->statusDescription = "Rabbit Connection Failed";
            $response->data = [];
        } else {
            if (is_array($payload)) {
                $payload = json_encode($payload);
            }

            try {
                $channel = $conn->channel();
                $channel->queue_declare($queueName, false, true, false, false);
                $channel->exchange_declare($exchangeKey, 'direct', false, true, false);
                $channel->queue_bind($queueName, $exchangeKey);
                $base->getLogFile()->addInfo(__LINE__ . ":" . __CLASS__ . " | "
                        . "QUEUE SERVICE:: Message:{$payload}");

                $outMsg = new AMQPMessage($payload, array('delivery_mode' => 2));
                $channel->basic_publish($outMsg, $exchangeKey, $routeKey);
                $channel->close();
                $conn->close();

                $response->code = 200;
                $response->statusDescription = "Successfully published on Queue:$queueName";
                $response->data = [
                    "queue" => $queueName,
                    "status" => "success",
                    "time" => date("Y-m-d H:i:s")
                ];
            } catch (Exception $ex) {
                $base->getLogFile()->addEmergency(__LINE__ . ":" . __CLASS__
                        . " | QUEUE SERVICE:: Exception on Publishing Message "
                        . "::" . json_encode($ex));
                $response->code = 500;
                $response->statusDescription = $ex->getMessage();
                $response->data = [];
            }
        }

        $stop = $base->getMicrotime() - $start;
        $base->getLogFile()->addInfo(__LINE__ . ":" . __CLASS__
                . " | TAT:$stop Seconds"
                . " | QUEUE SERVICE:: SERVICE RESPONSE<<<>>>" . json_encode($response));

        return $response;
    }

    /**
     * Creates connection to the Rabbit MQ server
     * @param type $server
     * @param type $port
     * @param type $user
     * @param type $pass
     * @param type $vhost
     */
    private function createRabbitConnection($server = null, $port = null, $user = null
    , $pass = null, $vhost = "/") {
        $base = new base();

        $rabbitMQ = $this->queue;

        if ($server == null) {
            $server = $rabbitMQ['rabbitServer'];
        }

        if ($port == null) {
            $port = $rabbitMQ['rabbitPortNo'];
        }

        if ($user == null) {
            $user = $rabbitMQ['rabbitUser'];
        }

        if ($pass == null) {
            $pass = $rabbitMQ['rabbitPass'];
        }

        try {
            $connectionection = new AMQPConnection($server, $port, $user, $pass, $vhost);
            $base->getLogFile()->addInfo(__LINE__ . ":" . __CLASS__ . " | QUEUE "
                    . "SERVICE:: Rabbit Connection successfull !!");

            return $connectionection;
        } catch (Exception $ex) {
            $base->getLogFile()->addInfo(__LINE__ . ":" . __CLASS__
                    . " | QUEUE SERVICE:: Exception:" . $ex->getMessage());
            return null;
        }
    }

}
