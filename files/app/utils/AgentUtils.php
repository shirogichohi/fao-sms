<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Phalcon\Mvc\Controller;
use \Firebase\JWT\JWT;
use ControllerBase as base;

class AgentUtils extends Controller {

    /**
     * QueryUserToken
     * @param type $token
     * @param type $active
     * @return type
     * @throws Exception
     */
    public static function QueryUserToken($token, $active = true) {
        $base = new base();

        try {
            $sql = "";

            if ($active) {
                $sql.=" AND agents.status=1";
            }

            $sqlParams = [
                ':client_token' => $token];

            $result = $base->rawSelect($sql, $sqlParams);

            return isset($result[0]) ? $result[0] : false;
        } catch (Exception $ex) {
            $base->getLogFile()->emergency(__LINE__ . ":" . __CLASS__
                    . " | Exception::" . $ex->getMessage());
            throw $ex;
        }
    }

}
