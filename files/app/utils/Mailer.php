<?php

use Phalcon\Mvc\Controller;

/**
 * 
 */
class Mailer extends Controller {

    public $sender;
    public $sender_pass;
    public $smtp_host_ip = 'smtp.gmail.com';
    public $smtp_port_no = 465;
    public $from_details;
    public $email_to;
    public $email_cc;
    public $email_bcc;
    public $filepath;
    public $email_subject;
    public $email_message;
    public $decription;

    /**
     * SendEmailWithAttachments
     * @return type
     */
    public function SendEmailWithAttachments() {
        $base = new ControllerBase();
        try {
            require_once (__DIR__) . '/../../vendor/swiftmailer/swiftmailer/lib/swift_required.php';

            $host_ip = gethostbyname($this->smtp_host_ip);
            $transport = \Swift_SmtpTransport::newInstance($this->smtp_host_ip, 465, 'ssl')
                    ->setUsername($this->sender)
                    ->setPassword($this->sender_pass);

            if (!$this->from_details) {
                $this->from_details = [
                    'from' => 'stats@southwell.io',
                    'decription' => isset($this->decription) ? $this->decription : 'Core Platform Mail Bot'
                ];
            }

            foreach ($this->email_to as $value) {
                $this->email_to[$value] = $value;
            }

            if (empty($this->email_to)) {
                return ['code' => 2, 'message' => 'Email recipient is required!!', 'response' => null];
            }

            foreach ($this->email_cc as $value) {
                $this->email_cc[$value] = $value;
            }


            foreach ($this->email_bcc as $value) {
                $this->email_bcc[$value] = $value;
            }

            $mailer = \Swift_Mailer::newInstance($transport);
            $message = \Swift_Message::newInstance($this->email_subject)
                    ->setFrom(array($this->from_details['from'] => $this->from_details['decription']));

            $message->setTo($this->email_to);
            $message->setCc($this->email_cc);
            $message->setBcc($this->email_bcc);
            $message->setBody($this->email_message, 'text/html');

            if (!is_array($this->filepath)) {
                $this->filepath = explode(',', $this->filepath);
            }

            $i = 1;
            foreach ($this->filepath as $file) {
                if (!is_file($this->filepath)) {
                    $base->getLogFile("error")->addInfo(__LINE__ . ":" . __CLASS__ . " | File Not Ok");
                }
                
                if (filesize($file) > 1024 * 25) {
                    $file = $this->gzCompressFile($file);


                    $message->attach(
                            Swift_Attachment::fromPath($file)
                                    ->setFilename("attachment_$i.gz"));
                    continue;
                }

                $extension = "";
                $mime = mime_content_type($file);
                if ($mime == 'application/pdf') {
                    $extension = '.pdf';
                }

                if ($mime == 'application/csv' || $mime == 'text/csv') {
                    $extension = '.csv';
                }

                if ($mime == 'text/plain') {
                    $extension = '.txt';
                }

                $message->attach(
                        Swift_Attachment::fromPath($file)
                                ->setFilename("attachment_$i" . $extension));

                $base->getLogFile("error")->addInfo(__LINE__ . ":" . __CLASS__ . " | File Ok");
            }

            $result = $mailer->send($message);

            if ($result > 0) {
                return ['code' => 0
                    , 'message' => 'Email email with attachment sent!!'
                    , 'response' => $result];
            }

            return ['code' => 1
                , 'message' => 'Email email with attachment Not sent!!'
                , 'response' => $result];
        } catch (Exception $ex) {
            return ['code' => 2
                , 'message' => $ex->getMessage()
                , 'response' => null];
        }
    }

    /**
     * SendEmailWithoutAttachments
     * @return type
     */
    public function SendEmailWithoutAttachments() {
        $base = new ControllerBase();

        try {
            require_once (__DIR__) . '/../../vendor/swiftmailer/swiftmailer/lib/swift_required.php';

            $host_ip = gethostbyname($this->smtp_host_ip);
            $transport = \Swift_SmtpTransport::newInstance($this->smtp_host_ip, 465, 'ssl')
                    ->setUsername($this->sender)
                    ->setPassword($this->sender_pass);

            if (!$this->from_details) {
                $this->from_details = [
                    'from' => 'stats@southwell.io',
                    'decription' => isset($this->decription) ? $this->decription : 'Core Platform Mail Bot'
                ];
            }

            $mailer = \Swift_Mailer::newInstance($transport);
            $message = \Swift_Message::newInstance($this->email_subject)
                    ->setFrom(array($this->from_details['from'] => $this->from_details['decription']));

            if (!is_array($this->email_to)) {
                $this->email_to = explode(',', $this->email_to);
            }

            foreach ($this->email_to as $value) {
                $this->email_to[$value] = $value;
            }

            if (empty($this->email_to)) {
                return ['code' => 2, 'message' => 'Email recipient is required!!', 'response' => null];
            }

            $message->setTo($this->email_to);

            if ($this->email_cc) {
                if (!is_array($this->email_cc)) {
                    $this->email_cc = explode(',', $this->email_cc);
                }

                foreach ($this->email_cc as $value) {
                    $this->email_cc[$value] = $value;
                }

                $message->setCc($this->email_cc);
            }


            if ($this->email_bcc) {
                if (!is_array($this->email_bcc)) {
                    $this->email_bcc = explode(',', $this->email_bcc);
                }

                foreach ($this->email_bcc as $value) {
                    $this->email_bcc[$value] = $value;
                }

                $message->setBcc($this->email_bcc);
            }

            $message->setBody($this->email_message, 'text/html');

            $result = $mailer->send($message);

            if ($result > 0) {
                return ['code' => 0, 'message' => 'Email email without attachment sent!!', 'response' => $result];
            }

            return ['code' => 1, 'message' => 'Email email without attachment Not sent!!', 'response' => $result];
        } catch (Exception $ex) {
            return ['code' => 2, 'message' => $ex->getMessage(), 'response' =>
                ['cc' => $this->email_cc, 'bcc' => $this->email_bcc, 'to' => $this->email_to]];
        }
    }

    /**
     * GZIPs a file on disk (appending .gz to the name)
     *
     * From http://stackoverflow.com/questions/6073397/how-do-you-create-a-gz-file-using-php
     * Based on function by Kioob at:
     * http://www.php.net/manual/en/function.gzwrite.php#34955
     * 
     * @param string $source Path to file that should be compressed
     * @param integer $level GZIP compression level (default: 9)
     * @return string New filename (with .gz appended) if success, or false if operation fails
     */
    public function gzCompressFile($source, $level = 9) {
        $dest = $source . '.gz';
        $mode = 'wb' . $level;
        $error = false;
        if ($fp_out = gzopen($dest, $mode)) {
            if ($fp_in = fopen($source, 'rb')) {
                while (!feof($fp_in))
                    gzwrite($fp_out, fread($fp_in, 1024 * 512));
                fclose($fp_in);
            } else {
                $error = true;
            }
            gzclose($fp_out);
        } else {
            $error = true;
        }
        if ($error)
            return false;
        else
            return $dest;
    }

}
