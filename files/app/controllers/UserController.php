<?php

use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Transaction\Failed as TxFailed;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use JwtDecodeToken as decodeAuth;

class UserController extends ControllerBase {

    protected $infologger;
    protected $errorlogger;
    protected $payload;
    protected $moduleName;

    const SERVICE_ID = 15;
    const ADD_SYSTEM_USERS = 64;
    const VIEW_SYSTEM_USERS = 4;
    const EDIT_SYSTEM_USERS = 5;

    /**
     * createUsers
     * @return type
     */
    public function createUsers() {
        $request = new Request();
        $data = $request->getJsonRawBody();
        $this->infologger = $this->getLogFile('info');
        $this->errorlogger = $this->getLogFile('error');

        $regex = '/"apiKey":"[^"]*?"/';
        $string = (preg_replace($regex, '"apiKey":********', json_encode($data)) . PHP_EOL);
        $string = (preg_replace('/"national_id":"[^"]*?"/', '"national_id":****ID****', $string) . PHP_EOL);
        $this->infologger->info(__LINE__ . ":" . __CLASS__ . " | User Create "
                . "Request:" . ($string));

        $email_address = isset($data->email_address) ? $data->email_address : null;
        $companyName = isset($data->client_name) ? $data->client_name : null;
        $full_names = isset($data->full_names) ? $data->full_names : null;
        $referrer = isset($data->referrer) ? $data->referrer : null;
        $msisdnNew = isset($data->mobile_no) ? $data->mobile_no : null;
        $region = isset($data->region) ? $data->region : null;
        $date_of_birth = isset($data->date_of_birth) ? $data->date_of_birth : null;
        $national_id = isset($data->national_id) ? $data->national_id : null;
        $billing_modeId = isset($data->billing_modeId) ? $data->billing_modeId : null;
        $description = isset($data->description) ? $data->description : null;
        $age = round((time() - strtotime($date_of_birth)) / (3600 * 24 * 365.25));
        $ageBracket = 0;

        $secretKey = "55abe029fdebae5e1d417e2ffb2a003klkhka0cd8b54763051cef08bc55abe029";

        $len = rand(1000, 999999);
        //$dataInfo = $this->randStrGen($len);

        $payloadToken = [ 'data' => $len . "" . $this->now()];

        $token = md5($this->createNewAuthToken($payloadToken, $secretKey));

        if (!$token || !$msisdnNew || !$full_names) {
            return $this->unProcessable(__FUNCTION__ . ":" . __CLASS__);
        }
        if (!$companyName) {
            $companyName = $full_names;
        }

        if ($email_address != null) {
            if (!filter_var($email_address, FILTER_VALIDATE_EMAIL)) {
                return $this->dataError(__LINE__ . ":" . __CLASS__
                                , "Validation Error."
                                , ['code' => 422
                            , 'message' => "Invalid email. Kindly enter valid email address"]);
            }
        }

        if (!$companyName) {
            return $this->dataError(__LINE__ . ":" . __CLASS__
                            , "Validation Error."
                            , ['code' => 422
                        , 'message' => "Kindly enter company Name"]);
        }

        if ($date_of_birth != null) {
            $startAge = $age - 5;
            $endAge = $age + 5;
            $ageBracket = $startAge . " - " . $endAge;
        }

        if (!$billing_modeId) {
            $billing_modeId = 1; //pre-paid
        }

        if (!$description) {
            $description = "Add new company description";
        }
        if (!$msisdnNew) {
            return $this->dataError(__LINE__ . ":" . __CLASS__
                            , "Validation Error."
                            , ['code' => 422
                        , 'message' => 'Provide phone number.']);
        }

        $msisdn = $this->validateMobile($msisdnNew);
        if (!$msisdn) {
            return $this->dataError(__LINE__ . ":" . __CLASS__
                            , "Validation Error."
                            , ['code' => 422
                        , 'message' => "Invalid Phone Number Format"
                        . ". Kindly enter valid phone number"]);
        }

        if ($referrer != null) {
            if (!is_numeric($referrer)) {
                return $this->dataError(__LINE__ . ":" . __CLASS__
                                , "Validation Error."
                                , ['code' => 422
                            , 'message' => "Invalid Referrer provided"]);
            }

            $referrer = $this->validateMobile($referrer);
            $referrer = $this->verifyReferrerNO($referrer);
            if ($referrer < 1) {
                return $this->dataError(__LINE__ . ":" . __CLASS__
                                , "Validation Error."
                                , ['code' => 422
                            , 'message' => 'Referral Does not exist. ' . $referrer]);
            }

            $sql = "SELECT user_client_map.client_id FROM user_client_map inner join "
                    . "user where user.profile_id=:profile_id";
            $r = $this->rawSelect($sql, [':profile_id' => $this->payload['referrerId']]);

            if (!empty($r)) {
                //$pieces = explode(" ", $full_names);
                $contact_payload = [
                    'mobile' => $msisdn,
                    'fname' => $full_names,
                    'surname' => "",
                    'lname' => "",
                    'network' => $this->getMobileNetwork($msisdn),
                    'channel_name' => 'REFFERAL',
                    'paybill' => "Vaspro",
                    'client_id' => $r[0]['client_id'],
                    'date' => $this->now(),
                    'account_number' => "Vaspro",];

                $payments = new Payments();
                $cbState = $payments->ContactBuilder($contact_payload);
                $this->infologger->addInfo(__LINE__ . ":" . __CLASS__
                        . " | " . $msisdn . " - "
                        . " | ContactBuilder::$cbState");
            }
        } else {
            $this->payload['referrerId'] = $this->settings['Account']['defaultReferralID'];
        }

        if ($national_id != null) {
            if (!is_numeric($national_id)) {
                return $this->dataError(__LINE__ . ":" . __CLASS__
                                , "Validation Error."
                                , ['code' => 422
                            , 'message' => "Invalid ID. Kindly enter valid National ID"]);
            }
            /**
             * we can handle IPRS here in future
             */
        }



        $transactionManager = new TransactionManager();
        $dbTransaction = $transactionManager->get();
        try {
            $checkProfile = Profile::findFirst([
                        "msisdn =:msisdn:",
                        "bind" => [ "msisdn" => $msisdn],]);

            $profileId = isset($checkProfile->profile_id) ?
                    $checkProfile->profile_id : false;
            if (!$checkProfile) {
                $profile = new Profile();
                $profile->setTransaction($dbTransaction);
                $profile->created_at = $this->now();
                $profile->msisdn = $msisdn;
                $profile->status = 1;
                $profile->network = $this->getMobileNetwork($msisdn);
                if ($profile->save() === false) {
                    $errors = [];
                    $messages = $profile->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTransaction->rollback("Create profile failed. Reason" . json_encode($errors));
                }

                $profileId = $profile->profile_id;
            }

            $checkProfileAttribution = ProfileAttribution::findFirst([
                        "profile_id =:profile_id:",
                        "bind" => [
                            "profile_id" => $profileId],]);

            $attributionId = isset($checkProfileAttribution->id) ?
                    $checkProfileAttribution->id : false;
            if (!$checkProfileAttribution) {
                $ProfileAttribution = new ProfileAttribution();
                $ProfileAttribution->setTransaction($dbTransaction);
                $ProfileAttribution->profile_id = $profileId;
                $ProfileAttribution->name = $full_names;
                $ProfileAttribution->email_address = $email_address;
                $ProfileAttribution->region = $region;
                $ProfileAttribution->date_of_birth = $date_of_birth;
                $ProfileAttribution->national_id = $national_id;
                $ProfileAttribution->frequency_of_use = 1;
                $ProfileAttribution->created_at = $this->now();
                if ($ProfileAttribution->save() === false) {
                    $errors = [];
                    $messages = $ProfileAttribution->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTransaction->rollback("Create profile Attribution failed. Reason" . json_encode($errors));
                }

                $attributionId = $ProfileAttribution->id;
            }

            $verification_code = rand(1000, 9999);
            $password = $this->security->hash(md5($verification_code));

            $checkUser = User::findFirst([
                        "profile_id =:profile_id:",
                        "bind" => [ "profile_id" => $profileId],]);

            $user_id = isset($checkUser->user_id) ? $checkUser->user_id : false;
            if ($user_id) {
                $checkAccount = UserClientMap::findFirst([
                            "user_id =:user_id:",
                            "bind" => [ "user_id" => $user_id],]);
                $xacc = isset($checkAccount->user_mapId) ? $checkAccount->user_mapId : false;
                if ($xacc) {
                    if ($checkAccount->status == 2) {//account not verified
                        $checkLogins = UserLogin::findFirst([
                                    "user_id =:user_id:",
                                    "bind" => [
                                        "user_id" => $user_id],]);
                        $checkLogins->setTransaction($dbTransaction);
                        $checkLogins->verification_code = md5($verification_code);
                        if ($checkLogins->save() === false) {
                            $errors = [];
                            $messages = $checkLogins->getMessages();
                            foreach ($messages as $message) {
                                $e["statusDescription"] = $message->getMessage();
                                $e["field"] = $message->getField();
                                array_push($errors, $e);
                            }

                            $dbTransaction->rollback("Create user login update failed. Reason" . json_encode($errors));
                        }
                        /*
                        $outbox = new Outbox();
                        $outbox->setTransaction($dbTransaction);
                        $outbox->alert_type = 'AUTHENTICATION';
                        $outbox->campaign_id = 0;
                        $outbox->created_by = $checkAccount->user_mapId;
                        $outbox->extra_data = "";
                        $outbox->created_at = $this->now();
                        $outbox->instance_id = 0;
                        $outbox->priority = 0;
                        $outbox->processed = 1;
                        $outbox->msisdn = $msisdn;
                        $outbox->number_of_sends = 1;
                        $outbox->short_code = $this->settings['defaultSenderId'];
                        $outbox->network = $this->getMobileNetwork($msisdn);
                        $outbox->sent = 211;
                        $outbox->message = "Hello $companyName,\nYour activation code is ***. "
                                . "Verify your account to continue.";
                        if ($outbox->save() === false) {
                            $errors = [];
                            $messages = $outbox->getMessages();
                            foreach ($messages as $message) {
                                $e["statusDescription"] = $message->getMessage();
                                $e["field"] = $message->getField();
                                array_push($errors, $e);
                            }

                            $dbTransaction->rollback("Create Authentication "
                                    . "Outbox failed. Reason" . json_encode($errors));
                        }

                        $correlator = $outbox->outbox_id;

                        $dbTransaction->commit();

                        
                        Sms verification Code
                         
                        $payload = [
                            'type' => 'TRANS',
                            "MSISDN" => $msisdn,
                            'correlator' => $correlator,
                            "message" => "Hello $companyName,\nYour activation code is $verification_code. "
                            . "Verify your account to continue."];
                        $QueueMessageResponse = $this->QueueMessage($payload);

                        $data_array = [
                            'code' => 200,
                            'data' => ['verify_code' => $verification_code]];

                        if ($QueueMessageResponse) {
                            $data_array['message'] = 'Verification Code SMS sent';
                            return $this->success(__LINE__ . ":" . __CLASS__
                                            , "User Created Successfully", $data_array);
                        }
                        */

                        $data_array['message'] = 'Verification code couldn\'t be sent to user';
                        return $this->success(__LINE__ . ":" . __CLASS__
                                        , "User Created Successfully"
                                        , $data_array, true);
                    }
                }

                $data_array = [
                    'code' => 202,
                    'message' => 'User already Exists with mobile:' . $msisdn,
                    'data' => ['verify_code' => $verification_code]];

                return $this->success(__LINE__ . ":" . __CLASS__
                                , "User Exists"
                                , $data_array, true);
            }
            /**
             * Create user
             */
            $user = new User();
            $user->setTransaction($dbTransaction);
            $user->profile_id = $profileId;
            $user->email_address = $email_address;
            $user->full_names = $full_names;
            $user->password = $password;
            $user->created_at = $this->now();
            if ($user->save() === false) {
                $errors = [];
                $messages = $user->getMessages();
                foreach ($messages as $message) {
                    $e["statusDescription"] = $message->getMessage();
                    $e["field"] = $message->getField();
                    array_push($errors, $e);
                }

                $dbTransaction->rollback("Create User failed. Reason" . json_encode($errors));
            }

            $userId = $user->user_id;

            $UserLogin = new UserLogin();
            $UserLogin->setTransaction($dbTransaction);
            $UserLogin->created_at = $this->now();
            $UserLogin->user_id = $userId;
            $UserLogin->verification_code = md5($verification_code);
            if ($UserLogin->save() === false) {
                $errors = [];
                $messages = $UserLogin->getMessages();
                foreach ($messages as $message) {
                    $e["statusDescription"] = $message->getMessage();
                    $e["field"] = $message->getField();
                    array_push($errors, $e);
                }

                $dbTransaction->rollback("Create User Login failed. Reason" . json_encode($errors));
            }

            $checkClient = Clients::findFirst([
                        "client_email =:client_email:",
                        "bind" => [ "client_email" => $email_address],]);
            $check = isset($checkClient->client_id) ? true : false;
            if ($check) {
                $data_array = [
                    'code' => 202,
                    'message' => 'Client already Exists with email:' . $email_address];

                return $this->success(__LINE__ . ":" . __CLASS__
                                , "Client Exists"
                                , $data_array, true);
            }

            $clients = new Clients();
            $clients->setTransaction($dbTransaction);
            $clients->client_email = $email_address;
            $clients->client_name = $companyName;
            $clients->created_at = $this->now();
            $clients->created_by = $userId;
            $clients->description = $description;
            if ($clients->save() === false) {
                $errors = [];
                $messages = $clients->getMessages();
                foreach ($messages as $message) {
                    $e["statusDescription"] = $message->getMessage();
                    $e["field"] = $message->getField();
                    array_push($errors, $e);
                }

                $dbTransaction->rollback("Create Client failed. Reason" . json_encode($errors));
            }

            $client_id = $clients->client_id;
            /**
             * Create user referral
             */
            $ClientReferral = new ClientReferral();
            $ClientReferral->setTransaction($dbTransaction);
            $ClientReferral->client_id = $client_id;
            $ClientReferral->profile_id = $profileId;
            $ClientReferral->created_at = $this->now();
            if ($ClientReferral->save() === false) {
                $errors = [];
                $messages = $ClientReferral->getMessages();
                foreach ($messages as $message) {
                    $e["statusDescription"] = $message->getMessage();
                    $e["field"] = $message->getField();
                    array_push($errors, $e);
                }

                $dbTransaction->rollback("Create Client referrer failed. Reason" . json_encode($errors));
            }
            /**
             * Create user_client_map
             */
            $UserClientMap = new UserClientMap();
            $UserClientMap->setTransaction($dbTransaction);
            $UserClientMap->client_id = $client_id;
            $UserClientMap->user_id = $userId;
            $UserClientMap->created_by = $this->now();
            $UserClientMap->billing_modeId = $billing_modeId;
            $UserClientMap->api_token = $token;
            $UserClientMap->status = 2; //Unverified
            $UserClientMap->created_by = $userId;
            $UserClientMap->permission_acl = Users::UserPermissions();
            $UserClientMap->created_at = $this->now();
            if ($UserClientMap->save() === false) {
                $errors = [];
                $messages = $UserClientMap->getMessages();
                foreach ($messages as $message) {
                    $e["statusDescription"] = $message->getMessage();
                    $e["field"] = $message->getField();
                    array_push($errors, $e);
                }

                $dbTransaction->rollback("Create User Client Mappings failed. Reason" . json_encode($errors));
            }

            $user_mapId = $UserClientMap->user_mapId;

            $checkClientServices = ClientServices::findFirst([
                        "client_id =:client_id: AND service_id=:service_id:",
                        "bind" => [
                            "client_id" => $client_id, "service_id" => 2],]);

            $id = isset($checkClientServices->client_serviceId) ? $checkClientServices->client_serviceId : false;
            if (!$id) {
                $ClientServices = new ClientServices();
                $ClientServices->setTransaction($dbTransaction);
                $ClientServices->client_id = $client_id;
                $ClientServices->created_at = $this->now();
                $ClientServices->created_by = $userId;
                $ClientServices->service_id = 2; //Bulk default ServiceId
                $ClientServices->unit_cost = $this->settings['Account']['SmsUnitCost'];
                $ClientServices->discount = ($this->settings['Account']['SmsDiscount'] / 100);
                if ($ClientServices->save() === false) {
                    $errors = [];
                    $messages = $ClientServices->getMessages();
                    foreach ($messages as $message) {
                        $e["statusDescription"] = $message->getMessage();
                        $e["field"] = $message->getField();
                        array_push($errors, $e);
                    }

                    $dbTransaction->rollback("Create User Client services failed. Reason" . json_encode($errors));
                }
            }

            $defaultBalance = $this->settings['Account']['FreeBalance'];
            $defaultBonus = $this->settings['Account']['FreeBonus'];
            $defaultWalletAlertThrehold = $this->settings['Account']['WalletAlertThrehold'];

            /**
             * Create Account balance
             */
            $userWallet = new UserWallet();
            $userWallet->balance = $defaultBalance;
            $userWallet->user_mapId = $client_id;
            $userWallet->created_at = $this->now();
            if ($userWallet->save() === false) {
                $errors = [];
                $messages = $userWallet->getMessages();
                foreach ($messages as $message) {
                    $e["statusDescription"] = $message->getMessage();
                    $e["field"] = $message->getField();
                    array_push($errors, $e);
                }

                $dbTransaction->rollback("Create Client balance failed. Reason" . json_encode($errors));
            }

            $transaction = new Transactions();
            $transaction->setTransaction($dbTransaction);
            $transaction->amount = $defaultBalance;
            $transaction->created_at = $this->now();
            $transaction->description = 'Free Account SignUp Credits.';
            $transaction->reference_id = $userWallet->id;
            $transaction->reference_type_id = 4;
            $transaction->source = 'FREE_SIGNUP_CREDITS';
            $transaction->user_mapId = $user_mapId;
            if ($transaction->save() === false) {
                $errors = [];
                $messages = $transaction->getMessages();
                foreach ($messages as $message) {
                    $e["statusDescription"] = $message->getMessage();
                    $e["field"] = $message->getField();
                    array_push($errors, $e);
                }

                $dbTransaction->rollback("Create Transaction failed. Reason" . json_encode($errors));
            }

            /*
            $outbox = new Outbox();
            $outbox->setTransaction($dbTransaction);
            $outbox->alert_type = 'AUTHENTICATION';
            $outbox->campaign_id = 0;
            $outbox->created_by = $user_mapId;
            $outbox->extra_data = json_encode(['dlr' => $this->settings['mnoApps']['defaultDlrUrl']]);
            $outbox->created_at = $this->now();
            $outbox->instance_id = 0;
            $outbox->priority = 0;
            $outbox->processed = 1;
            $outbox->msisdn = $msisdn;
            $outbox->number_of_sends = 1;
            $outbox->short_code = $this->settings['defaultSenderId'];
            $outbox->network = $this->getMobileNetwork($msisdn);
            $outbox->sent = 211;
            $outbox->message = "Hello $companyName,\nYour activation code is ***. "
                    . "Verify your account to continue.";
            if ($outbox->save() === false) {
                $errors = [];
                $messages = $outbox->getMessages();
                foreach ($messages as $message) {
                    $e["statusDescription"] = $message->getMessage();
                    $e["field"] = $message->getField();
                    array_push($errors, $e);
                }

                $dbTransaction->rollback("Create Authentication Outbox failed. Reason" . json_encode($errors));
            }

            $correlator = $outbox->outbox_id;
            */

            $data_array = [
                'code' => 200,
                'data' => ['verify_code' => $verification_code]];

            $dbTransaction->commit();

            $mail = new Mailer();
            $mail->email_to = $email_address;
            $mail->decription = "Account Notifier";
            $mail->email_cc = $this->settings['MailSettings']['WebMaster'] . "," . $this->settings['MailSettings']['SupportMaster'];
            $mail->sender = $this->settings['MailSettings']['AdminSender'];
            $mail->sender_pass = $this->settings['MailSettings']['AdminPass'];
            $mail->smtp_port_no = "465";
            $mail->email_subject = "Account Verification Code ";
            $mail->email_message = "<p>Hello $companyName, <br/>Your activation code is "
                    . "<strong>$verification_code</strong><br/>Verify your account to continue</p>";
            // $mail->filepath = "/";

            $mailResponse = $mail->SendEmailWithoutAttachments();
            $this->infologger->info(__LINE__ . ":" . __CLASS__
                    . " | SendEmailWithoutAttachments Response::" . json_encode($mailResponse));

            /*
             * Sms verification Code
             
            $payload = [
                'type' => 'TRANS',
                "MSISDN" => $msisdn,
                'correlator' => $correlator,
                "message" => "Hello $companyName,\nYour activation code is $verification_code. "
                . "Verify your account to continue."];
            $QueueMessageResponse = $this->QueueMessage($payload);

            if ($QueueMessageResponse) {
                $data_array['message'] = 'Verification Code emailed and sms sent';
                return $this->success(__LINE__ . ":" . __CLASS__
                                , "User Created Successfully", $data_array);
            }
            */

            $data_array['message'] = 'Verification code couldn\'t be sent to user';
            return $this->success(__LINE__ . ":" . __CLASS__
            , 'Account Created Successful'
            , $data_array);
        } catch (Exception $ex) {
            $this->errorlogger->emergency(__LINE__ . "::" . __CLASS__
                    . "Exception:" . $ex->getMessage());
            return $this->serverError(__LINE__ . ":" . __CLASS__
                            , "Internal Server Error.");
        }
    }

    

}
