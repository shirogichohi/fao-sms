<?php

class UserPermission extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $permission_id;

    /**
     *
     * @var string
     */
    public $permission;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var integer
     */
    public $module_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tableBanking");
        $this->setSource("user_permission");
        $this->belongsTo('module_id', 'Application\Models\Modules', 'id', ['alias' => 'Modules']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_permission';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserPermission[]|UserPermission|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserPermission|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
