<?php

class ClientMpesaPaybill extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $client_serviceId;

    /**
     *
     * @var string
     */
    public $paybill;

    /**
     *
     * @var string
     */
    public $store_number;

    /**
     *
     * @var string
     */
    public $organisation_name;

    /**
     *
     * @var double
     */
    public $org_acc_balance;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var string
     */
    public $message;

    /**
     *
     * @var integer
     */
    public $auto_respond;

    /**
     *
     * @var string
     */
    public $pushUrl;

    /**
     *
     * @var string
     */
    public $aggregated_on;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tableBanking");
        $this->setSource("client_mpesa_paybill");
        $this->hasMany('paybill', 'Application\Models\MpesaTransactions', 'paybill', ['alias' => 'MpesaTransactions']);
        $this->belongsTo('client_serviceId', 'Application\Models\ClientServices', 'client_serviceId', ['alias' => 'ClientServices']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'client_mpesa_paybill';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ClientMpesaPaybill[]|ClientMpesaPaybill|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ClientMpesaPaybill|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
