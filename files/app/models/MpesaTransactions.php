<?php

class MpesaTransactions extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $payment_id;

    /**
     *
     * @var integer
     */
    public $msisdn;

    /**
     *
     * @var string
     */
    public $reciept_number;

    /**
     *
     * @var string
     */
    public $account_number;

    /**
     *
     * @var double
     */
    public $amount;

    /**
     *
     * @var string
     */
    public $currency;

    /**
     *
     * @var string
     */
    public $paybill;

    /**
     *
     * @var string
     */
    public $first_name;

    /**
     *
     * @var string
     */
    public $surname;

    /**
     *
     * @var string
     */
    public $other_name;

    /**
     *
     * @var string
     */
    public $extra_data;

    /**
     *
     * @var string
     */
    public $payer_narration;

    /**
     *
     * @var string
     */
    public $payment_date;

    /**
     *
     * @var string
     */
    public $created_by;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tableBanking");
        $this->setSource("mpesa_transactions");
        $this->belongsTo('paybill', 'Application\Models\ClientMpesaPaybill', 'paybill', ['alias' => 'ClientMpesaPaybill']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'mpesa_transactions';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return MpesaTransactions[]|MpesaTransactions|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return MpesaTransactions|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
