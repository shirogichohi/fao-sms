<?php

class LoanRequest extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $request_id;

    /**
     *
     * @var integer
     */
    public $user_mapId;

    /**
     *
     * @var double
     */
    public $request_amount;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var integer
     */
    public $period;

    /**
     *
     * @var string
     */
    public $app_id;

    /**
     *
     * @var string
     */
    public $unique_id;

    /**
     *
     * @var double
     */
    public $interest_rate;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tableBanking");
        $this->setSource("loan_request");
        $this->hasMany('request_id', 'Application\Models\LoanGuarantee', 'request_id', ['alias' => 'LoanGuarantee']);
        $this->hasMany('request_id', 'Application\Models\LoanRequestStatus', 'request_id', ['alias' => 'LoanRequestStatus']);
        $this->belongsTo('user_mapId', 'Application\Models\UserClientMap', 'user_mapId', ['alias' => 'UserClientMap']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'loan_request';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return LoanRequest[]|LoanRequest|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return LoanRequest|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
