<?php

class Service extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $service_id;

    /**
     *
     * @var string
     */
    public $service_name;

    /**
     *
     * @var string
     */
    public $service_description;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var integer
     */
    public $created_by;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tableBanking");
        $this->setSource("service");
        $this->hasMany('service_id', 'Application\Models\ClientServices', 'service_id', ['alias' => 'ClientServices']);
        $this->hasMany('service_id', 'Application\Models\Modules', 'service_id', ['alias' => 'Modules']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'service';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Service[]|Service|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Service|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
