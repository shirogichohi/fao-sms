<?php

class UserLogin extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $user_login_id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $verification_code;

    /**
     *
     * @var integer
     */
    public $successful_attempts;

    /**
     *
     * @var integer
     */
    public $failed_attempts;

    /**
     *
     * @var integer
     */
    public $cumlative_failed_attempts;

    /**
     *
     * @var string
     */
    public $last_successful_date;

    /**
     *
     * @var string
     */
    public $last_failed_attempt;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tableBanking");
        $this->setSource("user_login");
        $this->belongsTo('user_id', 'Application\Models\User', 'user_id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_login';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserLogin[]|UserLogin|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserLogin|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
