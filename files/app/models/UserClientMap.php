<?php

class UserClientMap extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $user_mapId;

    /**
     *
     * @var integer
     */
    public $client_id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var integer
     */
    public $billing_modeId;

    /**
     *
     * @var string
     */
    public $permission_acl;

    /**
     *
     * @var string
     */
    public $api_token;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var integer
     */
    public $created_by;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tableBanking");
        $this->setSource("user_client_map");
        $this->hasMany('user_mapId', 'Application\Models\LoanGuarantee', 'approved_by', ['alias' => 'LoanGuarantee']);
        $this->hasMany('user_mapId', 'Application\Models\LoanRequest', 'user_mapId', ['alias' => 'LoanRequest']);
        $this->hasMany('user_mapId', 'Application\Models\Transactions', 'user_mapId', ['alias' => 'Transactions']);
        $this->hasMany('user_mapId', 'Application\Models\UserAudit', 'user_mapId', ['alias' => 'UserAudit']);
        $this->hasMany('user_mapId', 'Application\Models\UserWallet', 'user_mapId', ['alias' => 'UserWallet']);
        $this->belongsTo('client_id', 'Application\Models\Clients', 'client_id', ['alias' => 'Clients']);
        $this->belongsTo('user_id', 'Application\Models\User', 'user_id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user_client_map';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserClientMap[]|UserClientMap|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserClientMap|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
