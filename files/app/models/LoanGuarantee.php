<?php

class LoanGuarantee extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $guarantee_id;

    /**
     *
     * @var integer
     */
    public $request_id;

    /**
     *
     * @var string
     */
    public $amount;

    /**
     *
     * @var integer
     */
    public $status;

    /**
     *
     * @var integer
     */
    public $approved_by;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tableBanking");
        $this->setSource("loan-guarantee");
        $this->belongsTo('approved_by', 'Application\Models\UserClientMap', 'user_mapId', ['alias' => 'UserClientMap']);
        $this->belongsTo('request_id', 'Application\Models\LoanRequest', 'request_id', ['alias' => 'LoanRequest']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'loan-guarantee';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return LoanGuarantee[]|LoanGuarantee|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return LoanGuarantee|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
