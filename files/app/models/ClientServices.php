<?php

class ClientServices extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $client_serviceId;

    /**
     *
     * @var integer
     */
    public $client_id;

    /**
     *
     * @var integer
     */
    public $service_id;

    /**
     *
     * @var double
     */
    public $unit_cost;

    /**
     *
     * @var double
     */
    public $discount;

    /**
     *
     * @var integer
     */
    public $created_by;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $updated_at;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("tableBanking");
        $this->setSource("client_services");
        $this->hasMany('client_serviceId', 'Application\Models\ClientMpesaPaybill', 'client_serviceId', ['alias' => 'ClientMpesaPaybill']);
        $this->belongsTo('client_id', 'Application\Models\Clients', 'client_id', ['alias' => 'Clients']);
        $this->belongsTo('service_id', 'Application\Models\Service', 'service_id', ['alias' => 'Service']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'client_services';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return ClientServices[]|ClientServices|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return ClientServices|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
